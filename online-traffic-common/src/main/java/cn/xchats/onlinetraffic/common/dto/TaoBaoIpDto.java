package cn.xchats.onlinetraffic.common.dto;

import lombok.Data;

/*
 *
 *@author teddy
 *@date 2018/4/28
 */
@Data
public class TaoBaoIpDto {
    private String code;
    private IpDto data;
}
