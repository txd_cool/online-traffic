package cn.xchats.onlinetraffic.common.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
 *
 *@author teddy
 *@date 2018/5/2
 */
@Data
public class ShopDto {

    private String id;

    private String category;

    private String url;

    private String parentId;

    private String userId;

    private Date orderTime;

    private List<CollectDto> collectDtoList=new ArrayList<>();

    private List<ComparisonInfoDto> comparisonInfoDtoList=new ArrayList<>();

}
