package cn.xchats.onlinetraffic.common.other.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/*
 *正则类
 *@author teddy
 *@date 2018/1/30
 */
public enum RegularUtil {
    getMethods;

    //中国香港手机号码8位数，5|6|8|9开头+7位任意数
    private static final String HK_PHONE_REGEXP = "^(5|6|8|9)\\d{7}$";
    private static final Pattern HK_PHONE_REGEXP_PATTERN = Pattern.compile(HK_PHONE_REGEXP);

    /**
     * 中国大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数
     * 此方法中前三位格式有：
     * 13+任意数
     * 15+除4的任意数
     * 18+任意数
     * 17+除9的任意数
     * 147
     */
    private static final String CHINA_PHONE_REGEXP = "^((13[0-9])|(15[^4])|(18[0-9])|(17[0-8])|(147))\\d{8}$";
    private static final Pattern CHINA_PHONE_REGEXP_PATTERN = Pattern.compile(CHINA_PHONE_REGEXP);

    //时间
    public static final String FORMAT_DATE_1 = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final String FORMAT_DATE_2 = "yyyy-MM-dd";
    public static final String FORMAT_DATE_3 = "yyyy-MM-dd HH:mm:ss";
    private static final Map<String, SimpleDateFormat> DATE_FORMAT_MAP = new HashMap<>();

    public String dateFormat(String format, Date date) {
        checkDateFormatMap(format);
        SimpleDateFormat dateFormat = DATE_FORMAT_MAP.get(format);
        return dateFormat.format(date);
    }

    public Date getCurrentDateAfterTime(int dayNumber){
        Calendar date = Calendar.getInstance();
        date.setTime(new Date());
        date.set(Calendar.DATE, date.get(Calendar.DATE) + dayNumber);
        return date.getTime();
    }

   /* public static void main(String[] args) {
        String s = RegularUtil.getMethods.dateFormat(RegularUtil.FORMAT_DATE_2,RegularUtil.getMethods.getCurrentDateAfterTime(188));
        System.out.println(s);
        System.out.println(RegularUtil.getMethods.getCurrentDateAfterTime(30).getTime());
    }*/

    public Date dateParse(String format, String date) {
        checkDateFormatMap(format);
        SimpleDateFormat dateFormat = DATE_FORMAT_MAP.get(format);
        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    private void checkDateFormatMap(String format){
        if (!DATE_FORMAT_MAP.containsKey(format)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat(format);
            DATE_FORMAT_MAP.put(format, dateFormat);
        }
    }

    /**
     * 校验中国的手机号格式是否正确
     *
     * @param str
     * @return
     */
    public boolean checkPhone(String str) {
        return isChinaPhoneLegal(str) || isHKPhoneLegal(str);
    }

    /**
     * 中国大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数
     * 此方法中前三位格式有：
     * 13+任意数
     * 15+除4的任意数
     * 18+任意数
     * 17+除9的任意数
     * 147
     */
    public boolean isChinaPhoneLegal(String str) {
        return CHINA_PHONE_REGEXP_PATTERN.matcher(str).matches();
    }

    /**
     * 中国香港手机号码8位数，5|6|8|9开头+7位任意数
     */
    public boolean isHKPhoneLegal(String str) {
        return HK_PHONE_REGEXP_PATTERN.matcher(str).matches();
    }
}
