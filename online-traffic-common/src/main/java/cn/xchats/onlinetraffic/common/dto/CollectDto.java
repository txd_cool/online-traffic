package cn.xchats.onlinetraffic.common.dto;

import lombok.Data;

/*
 *
 *@author teddy
 *@date 2018/4/16
 */
@Data
public class CollectDto {

    private String id;

    private String fieldName;//采集的字段名

    private String value;//采集的值

    private String category;//分类

    private String shopId;//分类

    private String order;//排序

}
