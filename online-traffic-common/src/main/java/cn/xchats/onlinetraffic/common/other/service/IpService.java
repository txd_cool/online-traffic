package cn.xchats.onlinetraffic.common.other.service;

import cn.xchats.onlinetraffic.common.dto.IpDto;
import cn.xchats.onlinetraffic.common.dto.TaoBaoIpDto;
import cn.xchats.onlinetraffic.common.other.http.java.JavaHttpService;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

/*
 *
 *@author teddy
 *@date 2018/4/28
 */
@SuppressWarnings("all")
public class IpService {

    private static final String tao_bao_ip_server_url = "http://ip.taobao.com/service/getIpInfo.php?ip=";

    public static String getIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteAddr();
            if (ip.equals("127.0.0.1")) {
                //根据网卡取本机配置的IP
                InetAddress inet = null;
                try {
                    inet = InetAddress.getLocalHost();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                ip = inet.getHostAddress();
            }
        }
        // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if (ip != null && ip.length() > 15) {
            if (ip.indexOf(",") > 0) {
                ip = ip.substring(0, ip.indexOf(","));
            }
        }
        return ip;
    }

    public static IpDto createTaoBaoIpDto(String ipAddr) {
        IpDto ipDto = null;
        try {
            ipDto = new IpDto();
            //请求ip接口
            TaoBaoIpDto result = JavaHttpService.loadJSON(tao_bao_ip_server_url + ipAddr, TaoBaoIpDto.class);
            if (null == result)
                return null;
            if (!result.getCode().equals("0"))
                return null;
            System.out.println(result);
            return result.getData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ipDto;
    }

}
