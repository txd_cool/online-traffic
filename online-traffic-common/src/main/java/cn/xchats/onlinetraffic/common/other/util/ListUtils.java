package cn.xchats.onlinetraffic.common.other.util;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
 *
 *@author teddy
 *@date 2018/5/2
 */
public enum ListUtils {

    getMethods;

    public Boolean isBlank(List source) {
        if (source == null || source.size() == 0)
            return true;
        return false;
    }

    public Boolean isNotBlank(List source) {
        if (source != null && source.size() > 0)
            return true;
        return false;
    }

    //获取一个对象集合中指定字段的集合
    public <T, T2> List<T2> singleFieldList(List<T> source, String key, Class<T2> type) {
        List<Object> list;
        try {
            if (source instanceof ArrayList) {
                list = source.getClass().newInstance();
                for (int i = 0; i < source.size(); i++) {
                    T entity = source.get(i);
                    Class<?> entityClass = entity.getClass();
                    Field entityField = entityClass.getDeclaredField(key);
                    entityField.setAccessible(true);
                    list.add(entityField.get(entity));
                }
            } else {
                list = source.getClass().newInstance();
                for (T entity : source) {
                    Class<?> entityClass = entity.getClass();
                    Field entityField = entityClass.getDeclaredField(key);
                    entityField.setAccessible(true);
                    list.add(entityField.get(entity));
                }
            }
        } catch (Exception e) {
            return null;
        }
        return (List<T2>) list;
    }

    //list转Map
    public <K, V, T, M extends Map> Map<K, V> ListConvertMap(List<T> source, Class<M> mapType, String propertyKey) {
        M m;
        try {
            if (null == mapType)
                return null;

            m = mapType.newInstance();
            if (null == source || 0 == source.size())
                return m;

            if (source instanceof ArrayList) {
                for (int i = 0; i < source.size(); i++) {
                    T entity = source.get(i);
                    Class<?> entityClass = entity.getClass();
                    Field entityField = entityClass.getDeclaredField(propertyKey);
                    entityField.setAccessible(true);
                    String conditionFieldValue = String.valueOf(entityField.get(entity));
                    m.put(conditionFieldValue, entity);
                }
            } else {
                for (T entity : source) {
                    Class<?> entityClass = entity.getClass();
                    Field entityField = entityClass.getDeclaredField(propertyKey);
                    entityField.setAccessible(true);
                    String conditionFieldValue = String.valueOf(entityField.get(entity));
                    m.put(conditionFieldValue, entity);
                }
            }
        } catch (Exception e) {
            return null;
        }
        return m;

    }

}
