package cn.xchats.onlinetraffic.common.other.thread;


import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/*
 *
 *@author teddy
 *@date 2018/5/3
 */
public enum ExecutorServiceUtil {
    getmethods;

    private static final Executor executor = Executors.newFixedThreadPool(4);

    public Executor getExecutor() {
        return executor;

    }
}
