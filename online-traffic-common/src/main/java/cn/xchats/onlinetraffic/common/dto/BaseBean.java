package cn.xchats.onlinetraffic.common.dto;

import java.io.Serializable;
import java.lang.reflect.Field;

/*
 *接口返回Bean的父类
 *@author teddy
 *@date 2018/1/31
 */
public class BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;

    protected String meta; //辅助信息

    public BaseBean() {
    }

    @SuppressWarnings("all")
    public BaseBean setFieldValue(String field, Object value) {
        String fieldKey = field;
        Object fieldValue = value;
        BaseBean bean = this;
        if (null == fieldKey || "".equals(fieldKey))
            return this;

        try {
            Class<? extends BaseBean> beanClass = bean.getClass();
            Field beanField = beanClass.getDeclaredField(fieldKey);
            beanField.setAccessible(true);
            beanField.set(bean, value);
        } catch (Exception e) {
            return bean;
        }
        return bean;
    }

    @SuppressWarnings("all")
    public <T extends BaseBean> T valueOf(Class<T> type) {
       /* T value = null;
        try {
            value = type.newInstance();
            if (value instanceof BaseBean)
                return (T) this;
            return value;
        } catch (Exception e) {
            return value;
        }*/
        type = null;
        return (T) this;
    }

    @SuppressWarnings("all")
   /* public <T extends Class> T makeClass() throws Exception {
        Class<? extends BaseBean> currentClass = this.getClass();
        StringBuilder builder = new StringBuilder(currentClass.getTypeName());
        builder.replace(currentClass.getTypeName().lastIndexOf(".") + 1, currentClass.getTypeName().lastIndexOf(".") + 1, "Proxy$");

        ClassPool pool = ClassPool.getDefault();
        System.out.println(currentClass.getTypeName());
        // 设置类搜索路径
        pool.appendClassPath(new ClassClassPath(currentClass));
        CtClass makeClass = pool.get(currentClass.getTypeName());

        Field[] currentClassFields = currentClass.getDeclaredFields();
        //去除序列化字段
        for (int i = 0; i < currentClassFields.length; i++) {
            String name = currentClassFields[i].getName();
            if (name.equals("serialVersionUID")) {
                int length=currentClassFields.length - i - 1;
                //删除后重排序
                System.arraycopy(currentClassFields,i+1,currentClassFields,i,length);
                currentClassFields[currentClassFields.length-1] = null;
                break;
            }
        }
        //currentClassFields = Arrays.copyOf(currentClassFields, currentClassFields.length + 1);

        //插入第一个下标
        System.arraycopy(currentClassFields, 0, currentClassFields, 0 + 1,
                currentClassFields.length - 1);
        currentClassFields[0] = currentClass.getSuperclass().getDeclaredField("meta");

        //currentClassFields[currentClassFields.length - 1] = currentClass.getSuperclass().getDeclaredField("meta");
        //缓存
        CtClass[] parameterCtClass = new CtClass[currentClassFields.length];
        for (int i = 0; i < parameterCtClass.length; i++) {
            parameterCtClass[i] = pool.get(currentClassFields[i].getType().getName());
        }

        //新的class参数
        CtClass[] newParameterCtClass;
        //赋值
        StringBuilder constructorBody = new StringBuilder();
*//*        for (int i = 0; i < parameterCtClass.length; i++) {
            constructorBody.append("{");
            newParameterCtClass = new CtClass[i + 1];
            for (int j = 0; j <= i; j++) {
                newParameterCtClass[j] = parameterCtClass[j];
                constructorBody.append("this." + currentClassFields[j].getName() + "=$" + (j + 1) + ";");
            }

            constructorBody.append("}");
            CtConstructor ctConstructor = new CtConstructor(newParameterCtClass, makeClass);
            ctConstructor.setModifiers(Modifier.PUBLIC);
            ctConstructor.setBody(constructorBody.toString());
            makeClass.addConstructor(ctConstructor);
            constructorBody = new StringBuilder();
        }*//*

        //测试的新的规则
        List<String> ruleList = new ArrayList<>();
        StringBuilder rule = new StringBuilder();
        for (int i = 0; i < parameterCtClass.length; i++) { //控制参数数量
            //增加构造
            for (int j = 0; j < parameterCtClass.length; j++) { //控制组合
                //TODO bug.. 需修复
                for (int z = 0; z <= i; z++) {
                    rule.append(currentClassFields[z].getType().getName());
                }
                //重载的列表已存在
                if (ruleList.contains(rule.toString())) {
                    rule = new StringBuilder();
                    continue;
                }

                ruleList.add(rule.toString());
                rule = new StringBuilder();

                constructorBody.append("{");
                newParameterCtClass = new CtClass[i + 1];
                for (int y = 0; y <= i; y++) {
                    newParameterCtClass[y] = parameterCtClass[y];
                    constructorBody.append("this." + currentClassFields[y].getName() + "=$" + (y + 1) + ";");
                }
                constructorBody.append("}");
                CtConstructor ctConstructor = new CtConstructor(newParameterCtClass, makeClass);
                ctConstructor.setModifiers(Modifier.PUBLIC);
                ctConstructor.setBody(constructorBody.toString());
                CtConstructor[] constructors = makeClass.getConstructors();
                makeClass.addConstructor(ctConstructor);
                constructorBody = new StringBuilder();
                //数组元素第一位到最后一位
                CtClass fastParameterCtClass = parameterCtClass[0];
                int length1 = parameterCtClass.length - 1;
                System.arraycopy(parameterCtClass, 1, parameterCtClass, 0, length1);
                parameterCtClass[length1] = fastParameterCtClass;

                //数组元素第一位到最后一位
                Field fastCurrentClassField = currentClassFields[0];
                int length2 = parameterCtClass.length - 1;
                System.arraycopy(currentClassFields, 1, currentClassFields, 0, length2);
                currentClassFields[length2] = fastCurrentClassField;
            }
        }


        //-------------------end-----------------------------
        makeClass.setName(builder.toString());
        //makeClass.setSuperclass(pool.get("com.uteamtec.aerocardio.commons.types.bean.BaseBean"));
        //makeClass.writeFile("src\\main\\java\\" + builder.toString());
        return (T) makeClass.toClass();
    }*/


    public String getMeta() {
        return meta;
    }

    public void setMeta(String meta) {
        this.meta = meta;
    }
}
