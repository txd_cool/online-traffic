package cn.xchats.onlinetraffic.common.other.http.httpclient;

import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/*
 *
 *@author teddy
 *@date 2018/4/19
 */
public class HttpClientUtil {

    public static void searchImg() {
        try {
            CloseableHttpClient httpClient = null;
            HttpGet httpGet = null;
            CookieStore cookieStore = new BasicCookieStore();
            httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
            httpGet = new HttpGet("http://localhost:5500/search");
            httpClient.execute(httpGet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void searchInit() {
        try {
            CloseableHttpClient httpClient = null;
            HttpGet httpGet = null;
            CookieStore cookieStore = new BasicCookieStore();
            httpClient = HttpClients.custom().setDefaultCookieStore(cookieStore).build();
            httpGet = new HttpGet("http://localhost:5500/init");
            httpClient.execute(httpGet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 发送get请求
     * @param url    路径
     * @return
     */
    public static JSONObject httpGet(String url){
        //get请求返回结果
        JSONObject jsonResult = null;
        try {
            DefaultHttpClient client = new DefaultHttpClient();
            //发送get请求
            HttpGet request = new HttpGet(url);
            HttpResponse response = client.execute(request);

            /**请求发送成功，并得到响应**/
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                /**读取服务器返回过来的json字符串数据**/
                String strResult = EntityUtils.toString(response.getEntity());
                /**把json字符串转换成json对象**/
                jsonResult = JSONObject.parseObject(strResult);
                url = URLDecoder.decode(url, "UTF-8");
            } else {
                System.out.println("get请求提交失败:" + url);
                System.out.println("code >" +response.getStatusLine().getStatusCode());
            }
        } catch (IOException e) {
            System.out.println("get请求提交失败:" + url);
        }
        return jsonResult;
    }

    //第一次获取网页源码
    public static String getHtmlByUrl(String url) throws IOException{
        String html = null;
        CloseableHttpClient httpClient = HttpClients.createDefault();//创建httpClient对象
        HttpGet httpget = new HttpGet(url);
        try {
            HttpResponse responce = httpClient.execute(httpget);
            int resStatu = responce.getStatusLine().getStatusCode();
            if (resStatu == HttpStatus.SC_OK) {

                HttpEntity entity = responce.getEntity();
                if (entity != null) {
                    html = EntityUtils.toString(entity);//获得html源代码
                }
            }
        } catch (Exception e) {
            System.out.println("访问【"+url+"】出现异常!");
            e.printStackTrace();
        } finally {
            //释放连接
            httpClient.close();
        }
        return html;
    }

    public static String httpPost(String url) throws IOException{
        String html = null;
        CloseableHttpClient httpClient = HttpClients.createDefault();//创建httpClient对象
        //参数
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("name", "teddy"));
        nvps.add(new BasicNameValuePair("sex", "男"));
        HttpPost httpPost=new HttpPost(url);
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
        httpPost.setHeader("token","xxxxxxxxxxxxxxxxxxxxxxxxxxx");
        try {
            HttpResponse responce = httpClient.execute(httpPost);
            int resStatu = responce.getStatusLine().getStatusCode();
            System.out.println("code >>>>>>>>>>>>>>>>>>>>>>>>>>" + resStatu);
            if (resStatu == HttpStatus.SC_OK) {

                HttpEntity entity = responce.getEntity();
                if (entity != null) {
                    html = EntityUtils.toString(entity);//获得html源代码
                    for (int i=0;i<20;i++)
                        System.out.println("==========================================================");
                    System.out.println("value >>>>>>>>>>>>>>>>" +html);
                }
            }
        } catch (Exception e) {
            System.out.println("访问【"+url+"】出现异常!");
            e.printStackTrace();
        } finally {
            //释放连接
            httpClient.close();
        }
        return html;
    }

    public static void main(String[] args)  throws IOException{
        String result = HttpClientUtil.httpPost("http://localhost:2288/v1/test");
        System.out.println(result);
    }

}
