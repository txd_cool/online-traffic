package cn.xchats.onlinetraffic.common.type;

/**
 * 事件
 */
public enum Event {

    getMethods;

    //0 ~ 9
    public static final String _0 = "0";
    public static final String _1 = "1";
    public static final String _2 = "2";
    public static final String _3 = "3";
    public static final String _4 = "4";
    public static final String _5 = "5";
    public static final String _6 = "6";
    public static final String _7 = "7";
    public static final String _8 = "8";
    public static final String _9 = "9";

    //A ~ Z
    public static final String _A = "A";
    public static final String _B = "B";
    public static final String _C = "C";
    public static final String _D = "D";
    public static final String _E = "E";
    public static final String _F = "F";
    public static final String _G = "G";
    public static final String _H = "H";
    public static final String _I = "I";
    public static final String _J = "J";
    public static final String _K = "K";
    public static final String _L = "L";
    public static final String _M = "M";
    public static final String _N = "N";
    public static final String _O = "O";
    public static final String _P = "P";
    public static final String _Q = "Q";
    public static final String _R = "R";
    public static final String _S = "S";
    public static final String _T = "T";
    public static final String _U = "U";
    public static final String _V = "V";
    public static final String _W = "W";
    public static final String _X = "X";
    public static final String _Y = "Y";
    public static final String _Z = "Z";

    //F1 ~ F12
    public static final String _F1 = "F1";
    public static final String _F2 = "F2";
    public static final String _F3 = "F3";
    public static final String _F4 = "F4";
    public static final String _F5 = "F5";
    public static final String _F6 = "F6";
    public static final String _F7 = "F7";
    public static final String _F8 = "F8";
    public static final String _F9 = "F9";
    public static final String _F10 = "F10";
    public static final String _F11 = "F11";
    public static final String _F12 = "F12";

    // Home键
    public static final String _HOME = "HOME";
    // End键
    public static final String _END = "END";
    // page up键
    public static final String _PGUP = "PGUP";
    // page down键
    public static final String _PGDN = "PGDN";
    // 上箭头
    public static final String _UP = "UP";
    // 下箭头
    public static final String _DOWN = "DOWN";
    // 左箭头
    public static final String _LEFT  = "LEFT";
    // 右箭头
    public static final String _RIGHT = "RIGHT";
    // Esc键
    public static final String _ESCAPE  = "ESCAPE";
    // Tab键
    public static final String _TAB= "TAB";
    //  控制键
    public static final String _CONTROL= "CONTROL";
    //  shift键
    public static final String _SHIFT= "SHIFT";
    // 退格键
    public static final String _BACK_SPACE= "BACK_SPACE";
    // 大小写锁定键
    public static final String _CAPS_LOCK= "CAPS_LOCK";
    // 小键盘锁定键
    public static final String _NUM_LOCK= "NUM_LOCK";
    // 回车键
    public static final String _ENTER   = "ENTER";





























}
