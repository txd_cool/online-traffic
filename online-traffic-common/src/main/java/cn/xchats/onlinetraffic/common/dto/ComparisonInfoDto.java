package cn.xchats.onlinetraffic.common.dto;

import lombok.Data;

/*
 *
 *@author teddy
 *@date 2018/4/18
 */
@Data
public class ComparisonInfoDto {

    private String id;
    private String price;//价格
    private String dealCnt;//付款人数
    private String title;//标题
    private String location;//发货地
    private String shop;//店铺名称
    private String img;//主图
    private String url;//连接
    private String time;//诚信通会员时间
    private String shopId;
    private String userId;
}
