package cn.xchats.onlinetraffic.common.other.mq;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *@author teddy
 *@date 2018/5/7
 */
@Service
public class AMQPServer {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    public void send(String queueName, Object message) {
        this.rabbitTemplate.convertAndSend(queueName, message);
    }

    public void sendExchange(String exchange, String queueKey, Object... message) {
        this.rabbitTemplate.convertAndSend(exchange, queueKey, generateParam(message));
    }

    public void sendExchange(String exchange, String queueKey, Object message) {
        this.rabbitTemplate.convertAndSend(exchange, queueKey, message);
    }

    /**
     * 包装参数为map发送
     *
     * @param queueName
     * @param mapMessage key,value ...
     */
    public void send(String queueName, Object... mapMessage) {
        this.rabbitTemplate.convertAndSend(queueName, generateParam(mapMessage));
    }

    public Map generateParam(Object... mapMessage) {
        Map<Object, Object> param = new HashMap<>();
        if (mapMessage.length % 2 != 0)
            return param;
        for (int i = 0; i < mapMessage.length; i++) {
            Object key = mapMessage[i];
            Object value = mapMessage[++i];
            param.put(key, value);
        }
        return param;
    }
}
