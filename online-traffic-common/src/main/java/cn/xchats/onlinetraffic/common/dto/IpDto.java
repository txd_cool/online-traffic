package cn.xchats.onlinetraffic.common.dto;

import lombok.Data;

/*
 *
 *@author teddy
 *@date 2018/4/28
 */
@Data
public class IpDto {

    private String id;
    private String ip;//IP地址
    private String country;//国家
    private String region;//省
    private String city;//市
    private String county; //区
    private String isp;//网络运营商
    private String userInfoId;//用户信息id
    private Long loginTime;//登录时间
   // private String country_id;
   // private String region_id;
   // private String city_id;
   // private String county_id;
    //private String isp_id;

}
