package cn.xchats.onlinetraffic.common.dto;

import lombok.Data;

/*
 *
 *@author teddy
 *@date 2018/4/16
 */
@Data
public class CollectCategoryDto {

    private String id;

    private String fieldName;//采集的字段名

    private String executeJavascript;//要执行的js代码

    private String category;//分类
}
