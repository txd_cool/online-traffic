package cn.xchats.onlinetraffic.common.dto;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *@author teddy
 *@date 2018/4/28
 */
@Data
public class UserInfoDto {

    private String id;
    private Map<Long,IpDto> ip=new HashMap<>();//ip: key：时间  value：ip实体
    private String userId;
    private String userName;
    private Long size=0L;
    private String shopSort;
    private String emailPush;
    private Long expirationTime;
    private String expirationTimeFormat;
}
