package cn.xchats.onlinetraffic.common.dto;

import lombok.Data;

/*
 *
 *@author teddy
 *@date 2018/5/2
 */
@Data
public class FieldsDto {
    private String id;
    private String fieldName;
    private String order;
}
