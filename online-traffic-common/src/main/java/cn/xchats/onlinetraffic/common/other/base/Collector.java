package cn.xchats.onlinetraffic.common.other.base;

import cn.xchats.onlinetraffic.common.other.service.BaseService;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

/*
 *
 *@author teddy
 *@date 2018/4/16
 */
@SuppressWarnings("all")
public class Collector extends BaseService {

    protected <T> T getFieldValue(WebDriver webDriver,String executeJavascript){
        JavascriptExecutor webDriverJS = (JavascriptExecutor) webDriver;
        System.out.println(executeJavascript);
        //String js2="var number=''; var length= $(\".obj-content img\").length; for(var i=0;i<length;i++){number+=$(\".obj-content img\")[i].src+';'} return number;";
        //String result = String.valueOf(webDriverJS.executeScript(js2));
        //System.out.println(result);
        return (T)String.valueOf(webDriverJS.executeScript(executeJavascript));
    }

}
