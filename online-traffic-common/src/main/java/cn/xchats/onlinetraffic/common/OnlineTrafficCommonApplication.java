package cn.xchats.onlinetraffic.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineTrafficCommonApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineTrafficCommonApplication.class, args);
	}
}
