package cn.xchats.onlinetraffic.common.type;

/*
 *
 *@author teddy
 *@date 2018/5/7
 */
public interface AMQPType {
    //1688主页
    String LIST_HOME_1688=CollectType.list_home_1688;
    //淘宝主页
    String LIST_HOME_TAO_BAO=CollectType.list_home_tao_bao;
    //1688详情页
    String LIST_DETAILS_1688=CollectType.list_details_1688;
    //淘宝详情页
    String LIST_DETAILS_TAO_BAO=CollectType.list_details_tao_bao;
    //采集主页
    String LIST_HOME=CollectType.list_home;

    //邮件
    String EMAIL_QUEUE="email_queue";
}
