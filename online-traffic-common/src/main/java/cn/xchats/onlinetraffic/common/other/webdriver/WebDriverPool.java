package cn.xchats.onlinetraffic.common.other.webdriver;

import cn.xchats.onlinetraffic.common.other.util.PropertyUtil;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*
 *
 *@author teddy
 *@date 2018/5/7
 */
//@Service
@SuppressWarnings("all")
public class WebDriverPool {
    private int phantomjsDriverPoolSize = Integer.valueOf(PropertyUtil.getProperty("webDriver.pool.phantomjs.size"));//池子大小
    private int chromeDriverPoolSize = Integer.valueOf(PropertyUtil.getProperty("webDriver.pool.chrome.size"));//池子大小

    private List<WebDriver> phantomjsDriverPool = new ArrayList<>(phantomjsDriverPoolSize);
    private List<WebDriver> chromeDriverPool = new ArrayList<>(chromeDriverPoolSize);

    private List<Integer> select_phantomjs = new LinkedList<>(); //0要阻塞
    private List<Integer> select_chrome = new LinkedList<>();//0要阻塞

    private PhantomJsUtil phantomJsUtil = new PhantomJsUtil();

    public WebDriverPool() {
        init();
    }

    /*public WebDriverPool(int phantomjsDriverPoolSize,int chromeDriverPoolSize) {
        this.phantomjsDriverPoolSize = phantomjsDriverPoolSize;
        this.chromeDriverPoolSize = chromeDriverPoolSize;
    }*/


    public void init() {
        //初始化phantomjsDriverPool
        for (int i = 0; i < phantomjsDriverPoolSize; i++) {
            phantomjsDriverPool.add(phantomJsUtil.getPhantomJs());
            phantomjsDriverPool.get(i).get("https://detail.1688.com");
            select_phantomjs.add(i);
        }
        System.err.println("phantomjsDriverPool  init  success ..!  size>>>" + phantomjsDriverPool.size());
        //初始化chromeDriverPool
        for (int i = 0; i < chromeDriverPoolSize; i++) {
            chromeDriverPool.add(phantomJsUtil.getChromeDriver());
            //chromeDriverPool.get(i).get("https://detail.1688.com");
            select_chrome.add(i);
        }
        System.err.println("chromeDriverPool  init  success ..!  size>>>" + chromeDriverPool.size());
    }

    //选取一个可用的webdriver
    public synchronized int select_phantomjs(){
        while (true){
            if (select_phantomjs.size()!=0){
                int result= select_phantomjs.get(0);//每次选取第一个
                select_phantomjs.remove(0);
                return result;
            }
        }
    }

    public synchronized int select_chrome(){
        while (true){
            if (select_chrome.size()!=0){
                int result= select_chrome.get(0);//每次选取第一个
                select_chrome.remove(0);
                return result;
            }
        }
    }

    public synchronized void close_chrome(WebDriver webDriver){
        for (int i=0;i<chromeDriverPool.size();i++){
            if (webDriver==chromeDriverPool.get(i)){
                select_chrome.add(i);
                break;
            }
        }
    }

    public synchronized void close_phantomjs(WebDriver webDriver){
        for (int i=0;i<phantomjsDriverPool.size();i++){
            if (webDriver==phantomjsDriverPool.get(i)){
                select_phantomjs.add(i);
                break;
            }
        }
    }

    public WebDriver getChromeDriver() {
        WebDriver webDriver = chromeDriverPool.get(select_chrome());
        return webDriver;
    }

    public void test(){
        WebDriver chromeDriver1 = getChromeDriver();
        chromeDriver1.get("https://www.baidu.com/");
        close_chrome(chromeDriver1);

        WebDriver chromeDriver2 = getChromeDriver();
        chromeDriver2.get("https://www.baidu.com/");
        close_chrome(chromeDriver2);

        WebDriver chromeDriver3 = getChromeDriver();
        chromeDriver3.get("https://www.1.com/");
        close_chrome(chromeDriver3);

        WebDriver chromeDriver4 = getChromeDriver();
        chromeDriver4.get("https://www.2.com/");

        WebDriver chromeDriver5 = getChromeDriver();
        chromeDriver5.get("https://www.baidu.com/");
        close_chrome(chromeDriver5);
        close_chrome(chromeDriver4);
        System.out.println("......");
    }
}

