package cn.xchats.onlinetraffic.common.type;

/*
 *
 *@author teddy
 *@date 2018/6/8
 */
public interface EmailParameterType {

    String TARGET_EMAIL="targetEmail";
    String CONTENT="content";

}
