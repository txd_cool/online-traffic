package cn.xchats.onlinetraffic.common.dto;

import lombok.Data;

/*
 *
 *@author teddy
 *@date 2018/4/27
 */
@Data
public class UserDto  {

    private String id;//id号
    private String userName;//登录账号
    private String passWord;//密码
    private String telephone;//手机号
    private String name;//姓名
    private String address;//地址
    private String sex;//性别
    private String email;//电子邮箱
    private String qq;//qq号
    private String weChat;//微信号
    private String headPortrait;//头像

}
