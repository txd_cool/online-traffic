package cn.xchats.onlinetraffic.common.other.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


/**
 * 配置文件读取工具类
 * @author teddy
 *
 */
public class PropertiesUtil {
	private static Properties properties = null;

	public static Properties newInstance(String fileName) {
		properties = new Properties();
		try {
			InputStream input = PropertiesUtil.class
					.getResourceAsStream(fileName);
			properties.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return properties;
	}

	public static Properties getProperties() {
		return properties;
	}

	public static void setProperties(Properties pro) {
		properties = pro;
	}
}
