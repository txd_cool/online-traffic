package cn.xchats.onlinetraffic.common.type;

public enum CollectType {

    //1688主页
    LIST_HOME_1688,
    //淘宝主页
    LIST_HOME_TAO_BAO,
    //1688详情页
    LIST_DETAILS_1688,
    //淘宝详情页
    LIST_DETAILS_TAO_BAO,
    //采集主页
    LIST_HOME;

    public static final String list_home_1688="LIST_HOME_1688";
    public static final String list_home_tao_bao="LIST_HOME_TAO_BAO";
    public static final String list_details_1688="LIST_DETAILS_1688";
    public static final String list_details_tao_bao="LIST_DETAILS_TAO_BAO";
    public static final String list_home="LIST_HOME";

    //msg
    public static final String list_home_1688_msg="1688主页";
    public static final String list_home_tao_bao_msg="淘宝&天猫主页";
    public static final String list_details_1688_msg="1688详情页";
    public static final String list_details_tao_bao_msg="淘宝&天猫详情页";
    public static final String list_home_msg="采集主页";


}
