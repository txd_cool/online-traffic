/*
 * This file is generated by jOOQ.
*/
package cn.xchats.onlinetraffic.data.server.repository.tables;


import cn.xchats.onlinetraffic.data.server.repository.Indexes;
import cn.xchats.onlinetraffic.data.server.repository.Keys;
import cn.xchats.onlinetraffic.data.server.repository.OnlineTraffic;
import cn.xchats.onlinetraffic.data.server.repository.tables.records.CollectRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Collect extends TableImpl<CollectRecord> {

    private static final long serialVersionUID = 1295521044;

    /**
     * The reference instance of <code>online-traffic.collect</code>
     */
    public static final Collect COLLECT = new Collect();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<CollectRecord> getRecordType() {
        return CollectRecord.class;
    }

    /**
     * The column <code>online-traffic.collect.id</code>.
     */
    public final TableField<CollectRecord, String> ID = createField("id", org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false), this, "");

    /**
     * The column <code>online-traffic.collect.fieldName</code>. 采集的字段名
     */
    public final TableField<CollectRecord, String> FIELDNAME = createField("fieldName", org.jooq.impl.SQLDataType.VARCHAR(255).nullable(false), this, "采集的字段名");

    /**
     * The column <code>online-traffic.collect.value</code>. 采集的值
     */
    public final TableField<CollectRecord, String> VALUE = createField("value", org.jooq.impl.SQLDataType.VARCHAR(5000).nullable(false), this, "采集的值");

    /**
     * The column <code>online-traffic.collect.category</code>. 分类
     */
    public final TableField<CollectRecord, String> CATEGORY = createField("category", org.jooq.impl.SQLDataType.VARCHAR(255).nullable(false), this, "分类");

    /**
     * The column <code>online-traffic.collect.shopId</code>. 商品id
     */
    public final TableField<CollectRecord, String> SHOPID = createField("shopId", org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false), this, "商品id");

    /**
     * The column <code>online-traffic.collect.order</code>. 排序
     */
    public final TableField<CollectRecord, String> ORDER = createField("order", org.jooq.impl.SQLDataType.VARCHAR(255).nullable(false), this, "排序");

    /**
     * Create a <code>online-traffic.collect</code> table reference
     */
    public Collect() {
        this(DSL.name("collect"), null);
    }

    /**
     * Create an aliased <code>online-traffic.collect</code> table reference
     */
    public Collect(String alias) {
        this(DSL.name(alias), COLLECT);
    }

    /**
     * Create an aliased <code>online-traffic.collect</code> table reference
     */
    public Collect(Name alias) {
        this(alias, COLLECT);
    }

    private Collect(Name alias, Table<CollectRecord> aliased) {
        this(alias, aliased, null);
    }

    private Collect(Name alias, Table<CollectRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return OnlineTraffic.ONLINE_TRAFFIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.COLLECT_PRIMARY, Indexes.COLLECT_SHOPID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<CollectRecord> getPrimaryKey() {
        return Keys.KEY_COLLECT_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<CollectRecord>> getKeys() {
        return Arrays.<UniqueKey<CollectRecord>>asList(Keys.KEY_COLLECT_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<CollectRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<CollectRecord, ?>>asList(Keys.COLLECT_IBFK_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collect as(String alias) {
        return new Collect(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collect as(Name alias) {
        return new Collect(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Collect rename(String name) {
        return new Collect(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Collect rename(Name name) {
        return new Collect(name, null);
    }
}
