/*
 * This file is generated by jOOQ.
*/
package cn.xchats.onlinetraffic.data.server.repository.tables.records;


import cn.xchats.onlinetraffic.data.server.repository.tables.Shop;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ShopRecord extends UpdatableRecordImpl<ShopRecord> implements Record6<String, String, String, String, String, Timestamp> {

    private static final long serialVersionUID = 1314472584;

    /**
     * Setter for <code>online-traffic.shop.id</code>.
     */
    public void setId(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>online-traffic.shop.id</code>.
     */
    public String getId() {
        return (String) get(0);
    }

    /**
     * Setter for <code>online-traffic.shop.category</code>. 商品分类
     */
    public void setCategory(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>online-traffic.shop.category</code>. 商品分类
     */
    public String getCategory() {
        return (String) get(1);
    }

    /**
     * Setter for <code>online-traffic.shop.url</code>. 商品链接
     */
    public void setUrl(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>online-traffic.shop.url</code>. 商品链接
     */
    public String getUrl() {
        return (String) get(2);
    }

    /**
     * Setter for <code>online-traffic.shop.parentId</code>. 关联的父类id
     */
    public void setParentid(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>online-traffic.shop.parentId</code>. 关联的父类id
     */
    public String getParentid() {
        return (String) get(3);
    }

    /**
     * Setter for <code>online-traffic.shop.userId</code>.
     */
    public void setUserid(String value) {
        set(4, value);
    }

    /**
     * Getter for <code>online-traffic.shop.userId</code>.
     */
    public String getUserid() {
        return (String) get(4);
    }

    /**
     * Setter for <code>online-traffic.shop.orderTime</code>. 排序
     */
    public void setOrdertime(Timestamp value) {
        set(5, value);
    }

    /**
     * Getter for <code>online-traffic.shop.orderTime</code>. 排序
     */
    public Timestamp getOrdertime() {
        return (Timestamp) get(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<String, String, String, String, String, Timestamp> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<String, String, String, String, String, Timestamp> valuesRow() {
        return (Row6) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field1() {
        return Shop.SHOP.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Shop.SHOP.CATEGORY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Shop.SHOP.URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return Shop.SHOP.PARENTID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field5() {
        return Shop.SHOP.USERID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field6() {
        return Shop.SHOP.ORDERTIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component2() {
        return getCategory();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component3() {
        return getUrl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component4() {
        return getParentid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String component5() {
        return getUserid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp component6() {
        return getOrdertime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getCategory();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getUrl();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getParentid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value5() {
        return getUserid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value6() {
        return getOrdertime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShopRecord value1(String value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShopRecord value2(String value) {
        setCategory(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShopRecord value3(String value) {
        setUrl(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShopRecord value4(String value) {
        setParentid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShopRecord value5(String value) {
        setUserid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShopRecord value6(Timestamp value) {
        setOrdertime(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShopRecord values(String value1, String value2, String value3, String value4, String value5, Timestamp value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ShopRecord
     */
    public ShopRecord() {
        super(Shop.SHOP);
    }

    /**
     * Create a detached, initialised ShopRecord
     */
    public ShopRecord(String id, String category, String url, String parentid, String userid, Timestamp ordertime) {
        super(Shop.SHOP);

        set(0, id);
        set(1, category);
        set(2, url);
        set(3, parentid);
        set(4, userid);
        set(5, ordertime);
    }
}
