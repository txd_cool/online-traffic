/*
 * This file is generated by jOOQ.
*/
package cn.xchats.onlinetraffic.data.server.repository.tables;


import cn.xchats.onlinetraffic.data.server.repository.Indexes;
import cn.xchats.onlinetraffic.data.server.repository.Keys;
import cn.xchats.onlinetraffic.data.server.repository.OnlineTraffic;
import cn.xchats.onlinetraffic.data.server.repository.tables.records.UserinfoRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Userinfo extends TableImpl<UserinfoRecord> {

    private static final long serialVersionUID = 489856445;

    /**
     * The reference instance of <code>online-traffic.userinfo</code>
     */
    public static final Userinfo USERINFO = new Userinfo();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<UserinfoRecord> getRecordType() {
        return UserinfoRecord.class;
    }

    /**
     * The column <code>online-traffic.userinfo.id</code>.
     */
    public final TableField<UserinfoRecord, String> ID = createField("id", org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false), this, "");

    /**
     * The column <code>online-traffic.userinfo.userId</code>.
     */
    public final TableField<UserinfoRecord, String> USERID = createField("userId", org.jooq.impl.SQLDataType.VARCHAR(100).nullable(false), this, "");

    /**
     * The column <code>online-traffic.userinfo.size</code>.
     */
    public final TableField<UserinfoRecord, Integer> SIZE = createField("size", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>online-traffic.userinfo.shop_sort</code>. 商品展示排序规则
     */
    public final TableField<UserinfoRecord, String> SHOP_SORT = createField("shop_sort", org.jooq.impl.SQLDataType.VARCHAR(100).defaultValue(org.jooq.impl.DSL.inline("asc", org.jooq.impl.SQLDataType.VARCHAR)), this, "商品展示排序规则");

    /**
     * The column <code>online-traffic.userinfo.email_push</code>. 开启邮件推送提醒
     */
    public final TableField<UserinfoRecord, String> EMAIL_PUSH = createField("email_push", org.jooq.impl.SQLDataType.VARCHAR(100).defaultValue(org.jooq.impl.DSL.inline("off", org.jooq.impl.SQLDataType.VARCHAR)), this, "开启邮件推送提醒");

    /**
     * The column <code>online-traffic.userinfo.expiration_time</code>. 账号使用期限
     */
    public final TableField<UserinfoRecord, Long> EXPIRATION_TIME = createField("expiration_time", org.jooq.impl.SQLDataType.BIGINT.nullable(false), this, "账号使用期限");

    /**
     * Create a <code>online-traffic.userinfo</code> table reference
     */
    public Userinfo() {
        this(DSL.name("userinfo"), null);
    }

    /**
     * Create an aliased <code>online-traffic.userinfo</code> table reference
     */
    public Userinfo(String alias) {
        this(DSL.name(alias), USERINFO);
    }

    /**
     * Create an aliased <code>online-traffic.userinfo</code> table reference
     */
    public Userinfo(Name alias) {
        this(alias, USERINFO);
    }

    private Userinfo(Name alias, Table<UserinfoRecord> aliased) {
        this(alias, aliased, null);
    }

    private Userinfo(Name alias, Table<UserinfoRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return OnlineTraffic.ONLINE_TRAFFIC;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.USERINFO_PRIMARY, Indexes.USERINFO_USERID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<UserinfoRecord> getPrimaryKey() {
        return Keys.KEY_USERINFO_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<UserinfoRecord>> getKeys() {
        return Arrays.<UniqueKey<UserinfoRecord>>asList(Keys.KEY_USERINFO_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<UserinfoRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<UserinfoRecord, ?>>asList(Keys.USERINFO_IBFK_1);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Userinfo as(String alias) {
        return new Userinfo(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Userinfo as(Name alias) {
        return new Userinfo(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Userinfo rename(String name) {
        return new Userinfo(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Userinfo rename(Name name) {
        return new Userinfo(name, null);
    }
}
