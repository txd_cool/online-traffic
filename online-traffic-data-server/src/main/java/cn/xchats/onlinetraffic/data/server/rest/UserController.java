package cn.xchats.onlinetraffic.data.server.rest;

import cn.xchats.onlinetraffic.common.other.service.IpService;
import cn.xchats.onlinetraffic.data.server.server.UserService;
import cn.xchats.onlinetraffic.jooq.frame.common.BaseBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/*
 *
 *@author teddy
 *@date 2018/5/2
 */
@RestController
@RequestMapping(value = "user")
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/loginUser")
    public BaseBean loginUser(HttpServletRequest request, String userName, String passWord) {
        String ipAddr = IpService.getIpAddr(request);
        System.out.println("ip>>>>>>>>>>"+ipAddr);
        return userService.loginUser(userName, passWord,ipAddr);
    }

    @RequestMapping("/registerUser")
    public BaseBean registerUser(String userName, String passWord, String telephone, String name, String address, String sex, String email, String qq, String weChat) {
        return userService.registerUser(userName, passWord, telephone, name, address, sex,email,qq,weChat);
    }

    @RequestMapping(value = "/updateUserSetter",method = RequestMethod.POST)
    public BaseBean updateUserSetter(@RequestParam String userId, @RequestParam String shopSort, @RequestParam String emailPush) {
        return userService.updateUserSetter(userId,shopSort,emailPush);
    }

    @RequestMapping(value = "/userInfo",method = RequestMethod.GET)
    public BaseBean updateUserSetter(@RequestParam String userId) {
        return userService.getUserInfo(userId);
    }

    @RequestMapping(value = "/updateUser",method = RequestMethod.POST)
    public BaseBean updateUser(String userId, String telephone, String name, String address, String sex, String email, String qq, String weChat) {
        return userService.updateUser(userId,telephone,name,address,sex,email,qq,weChat);
    }
    @RequestMapping(value = "/updateUserHeadPortrait",method = RequestMethod.POST)
    public BaseBean updateUserHeadPortrait(String userId, String headPortrait) {
        return userService.updateUserHeadPortrait(userId,headPortrait);
    }
}
