package cn.xchats.onlinetraffic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineTrafficWebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineTrafficWebappApplication.class, args);
	}
}
