package cn.xchats.onlinetraffic.webapp.controller;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/*
 *
 *@author teddy
 *@date 2018/4/13
 */
@Controller
@SuppressWarnings("all")
@RequestMapping("v1.0")
public class LoginController {

    @RequestMapping("/login")
    public String get(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "login";
    }

    @RequestMapping(value = "/addSession",method = RequestMethod.POST)
    @ResponseBody
    public void addSession(String userName,HttpServletRequest request){
        request.getSession().setAttribute("user",userName);
    }

    @RequestMapping(value = "/session",method = RequestMethod.GET)
    @ResponseBody
    public Object getSession(HttpServletRequest request){
        return request.getSession().getAttribute("user");
    }

    @RequestMapping(value = "/removeSession",method = RequestMethod.GET)
    public String removeSession(HttpServletRequest request){
        request.getSession().removeAttribute("user");
        return login(request);
    }

    @RequestMapping("/")
    public String login( HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "collectList";
    }

    @RequestMapping("/addCollectRulePage")
    public String addCollectRulePage(Model model, HttpServletRequest request) {
        if (true)
            return "err";
        return "addCollectRule.html";
    }

    @RequestMapping("/addField")
    public String addField(Model model, HttpServletRequest request) {
        if (true)
            return "err";
        return "addField.html";
    }

    @RequestMapping("/itemList")
    public String itemList(String category, String shopId, Model model) {
        String title = "同款信息展示页";
        if (CollectType.LIST_DETAILS_1688.name().equals(category)) {
            title = "1688同款信息展示页";
        }
        if (CollectType.LIST_DETAILS_TAO_BAO.name().equals(category)) {
            title = "淘宝&天猫同款信息展示页";
        }
        model.addAttribute("title", title);
        model.addAttribute("shopId", shopId);
        model.addAttribute("category", category);
        return "itemList";
    }

    @RequestMapping("/1688_list")
    public String list_1688(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "1688List";
    }

    @RequestMapping("/taobao_list")
    public String list_taobao(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "taobaoList";
    }

    //文档

    @RequestMapping("/index")
    public String index(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "index";
    }
    @RequestMapping("/consumerDocument")
    public String consumerDocument(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "consumerDocument";
    }
    @RequestMapping("/producerDocument")
    public String producerDocument(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "producerDocument";
    }
    @RequestMapping("/whyUseDocument")
    public String whyUseDocument(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "why-use-document";
    }
    @RequestMapping("/register")
    public String register(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "register";
    }
    @RequestMapping("/setter")
    public String setter(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "setter";
    }
    @RequestMapping("/updateUser")
    public String updateUser(Model model, HttpServletRequest request) {
        //collectorService.countRedisSize();
        return "updateUser";
    }
}
