package cn.xchats.onlinetraffic.webapp.config;

import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.stereotype.Component;

/*
 *
 *@author teddy
 *@date 2018/5/9
 */
@Component
@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 1800)
public class SessionConfig {


}
