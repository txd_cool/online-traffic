package cn.xchats.onlinetraffic.collect.amqp.server.service;


import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.*;
import cn.xchats.onlinetraffic.collect.amqp.server.util.WebDriverPool;
import cn.xchats.onlinetraffic.common.dto.*;
import cn.xchats.onlinetraffic.common.other.http.httpclient.HttpClientUtil;
import cn.xchats.onlinetraffic.common.other.http.java.DownLoadUtil;
import cn.xchats.onlinetraffic.common.other.mq.AMQPServer;
import cn.xchats.onlinetraffic.common.other.redis.RedisTemplateService;
import cn.xchats.onlinetraffic.common.other.util.ListUtils;
import cn.xchats.onlinetraffic.common.other.webdriver.PhantomJsUtil;
import cn.xchats.onlinetraffic.common.type.*;
import cn.xchats.onlinetraffic.jooq.frame.JooqBaseService;
import cn.xchats.onlinetraffic.jooq.frame.common.BaseBean;
import cn.xchats.onlinetraffic.jooq.frame.common.MessageBean;
import org.jooq.tools.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/*
 *
 *@author teddy
 *@date 2018/5/7
 */
@Service
@SuppressWarnings("all")
public class CollectorService extends JooqBaseService {

    @Autowired
    private AMQPServer amqpServer;

    @Autowired
    private RedisTemplateService redisTemplateService;

    @Autowired
    // @Lazy
    private WebDriverPool webDriverPool;

    //采集的字段名的key
    private static final String collectCategoryJsSplit = "javaSplit";

    private static final String[] categoryKeyArray = {"1688", "网商园"};

    private static final PhantomJsUtil phantomJsUtil = new PhantomJsUtil();

    //private static final ExecutorService executorService = Executors.newFixedThreadPool(1);

    private static boolean searchImgFlag = false;

    private static Object searchImgFlagLock = new Object();

    //pc
    private static final String pc_tmall = "https://detail.tmall.com/item.htm";
    private static final String pc_tao_bao = "https://item.taobao.com/item.htm";
    //iphone
    private static final String iphone_tmail = "https://detail.m.tmall.com";
    private static final String iphone_tao_bao = "https://h5.m.taobao.com";


    //添加首页采集信息
    @RabbitListener(queues = AMQPType.LIST_HOME)
    // @Async("executor_fixed_4")
    //String userId, CollectType shopCategory, CollectWebsiteType collectWebsiteType, String url
    public void saveCollectList(Map<Object, Object> param) {
        //参数
        String userId = String.valueOf(param.get("userId"));
        CollectType shopCategory = CollectType.valueOf(String.valueOf(param.get("shopCategory")));
        String collectWebsiteType = String.valueOf(param.get("collectWebsiteType"));
        String url = String.valueOf(param.get("url"));

        ShopDto shopDto = new ShopDto();
        shopDto.setId(UUID.randomUUID().toString());
        shopDto.setCategory(shopCategory.name());
        shopDto.setUrl(url);
        shopDto.setUserId(userId);
        shopDto.setOrderTime(new Date());
        long s = System.currentTimeMillis();
        WebDriver webDriver = null;
        try {
            webDriver = webDriverPool.getPhantomJs();//phantomJsUtil.getChromeDriver(50,50);
            //webDriver.get(URLType.home_1688);
            if (shopCategory.name().equals(CollectType.LIST_HOME_TAO_BAO.name())) {
                //获取域名之后的参数部分
                String urlParam = url.substring(url.indexOf("?"), url.length());
                //手机天猫
                if (url.contains(iphone_tmail)) {
                    url = pc_tmall + urlParam;
                }
                //手机淘宝
                else if (url.contains(iphone_tao_bao)) {
                    url = pc_tao_bao + urlParam;
                }
                //pc端
            } else if (shopCategory.name().equals(CollectType.LIST_HOME_1688.name())) {
                url = url.replace("m.1688.com", "detail.1688.com");
            }
            webDriver.get(url);
            if (shopCategory.name().equals(CollectType.LIST_HOME_TAO_BAO.name())) {
                phantomJsUtil.getJquery(webDriver);
            }

            //phantomJsUtil.getJquery(webDriver);
            //phantomJsUtil.scrollTop(webDriver, 4);
            List<CollectDto> collectDtoList = new ArrayList<>();
            //List<String> fieldList = ListUtils.getMethods.singleFieldList((ArrayList) getFieldList().valueOf(MessageBean.class).getData(), "fieldName", String.class);

            //查出获取元素的js代码集合
            List<CollectCategoryDto> collectCategoryDtos = jooqStorage.listByCondition(Collectcategory.COLLECTCATEGORY, CollectCategoryDto.class, Collectcategory.COLLECTCATEGORY.CATEGORY.eq(collectWebsiteType));
            Map<String, CollectCategoryDto> collectCategoryDtoMap = ListUtils.getMethods.ListConvertMap(collectCategoryDtos, HashMap.class, "fieldName");
            //查出排序好的字段
            MessageBean<List<FieldsDto>> messageBean = getFieldList().valueOf(MessageBean.class);
            //执行js代码块爬取数据
            for (int i = 0; i < messageBean.getData().size(); i++) {
                CollectCategoryDto collectCategoryDto = collectCategoryDtoMap.get(messageBean.getData().get(i).getFieldName());
                if (collectCategoryDto == null)
                    continue;

                //字段有js代码块则执行
                String[] javaScriptList = collectCategoryDto.getExecuteJavascript().split(collectCategoryJsSplit);
                CollectDto collectDto = new CollectDto();
                String fieldValue = "";
                for (int j = 0; javaScriptList != null && j < javaScriptList.length; j++) {
                    String javaScript = javaScriptList[j];
                    //数据库 " 存储会加\ 转义
                    javaScript = javaScript.replace("\\", "");
                    try {
                        fieldValue += getFieldValue(webDriver, javaScript);
                    } catch (Exception e) {
                        System.out.println("error-->");
                    }
                    System.out.println("fieldName>>>>" + collectCategoryDto.getFieldName() + ">>>>>>>>" + fieldValue);
                }
                fieldValue.replace("\\n", "").replace("\\r", "").replace(" ", "");
                collectDto.setId(UUID.randomUUID().toString());
                collectDto.setFieldName(collectCategoryDto.getFieldName());
                collectDto.setValue(fieldValue);
                collectDto.setShopId(shopDto.getId());
                collectDto.setCategory(shopCategory.name());
                collectDto.setOrder(String.valueOf(i));
                collectDtoList.add(collectDto);
            }
            //组装
            jooqStorage.create(Shop.SHOP, ShopDto.class, shopDto);
            jooqStorage.batchCreate(Collect.COLLECT, collectDtoList);
            System.out.println("saveCollectList 用时>>>" + (System.currentTimeMillis() - s));

            //是否发送邮件
            // TODO 是否发送邮件
            String title = null;
            String titleFieldName = "商品标题";
            for (int i = 0; i < collectDtoList.size(); i++) {
                if (titleFieldName.equals(collectDtoList.get(i).getFieldName())) {
                    //title = collectDtoList.get(i).getValue().split(";")[0];  //图片
                    title = collectDtoList.get(i).getValue();
                    break;
                }
            }
            IfVerifyBeEmailPush(userId, title, shopCategory);
        } finally {
            if (webDriver != null)
                //webDriver.quit();
                webDriverPool.close(webDriver);
        }
    }

    /**
     * 如果用户开启了邮件推送，则推送邮件至用户邮箱
     *
     * @param userId
     * @param imgUrl
     * @param type
     */
    public void IfVerifyBeEmailPush(String userId, String content, CollectType type) {
        //是否发送邮件
        if (verifyEmailPush(userId)) {
            //查找用户
            List<UserDto> userDtolist = jooqStorage.dslSelect(dsl -> dsl.select(User.USER.EMAIL).from(User.USER).where(User.USER.ID.eq(userId)), UserDto.class);
            if (ListUtils.getMethods.isBlank(userDtolist)) {
                return;
            }
            //推送邮件
            emailPush(content, type, userDtolist.get(0).getEmail());
        }
    }

    /**
     * 发送邮件推送
     *
     * @param imgUrl
     * @param type
     * @param content
     * @param targetEmail
     */
    public void emailPush(String content, CollectType type, String targetEmail) {
        //String sendContent = "您有一个#{0}任务已完成：请点击连接查看采集图片：#{1}";
        String sendContent = "您有一个#{0}任务已完成：#{1}";
        String msg_0;
        String msg_1 = content;
        switch (type.name()) {
            case CollectType.list_home_tao_bao:
                msg_0 = CollectType.list_home_tao_bao_msg;
                break;
            case CollectType.list_home_1688:
                msg_0 = CollectType.list_home_1688_msg;
                break;
            case CollectType.list_details_tao_bao:
                msg_0 = CollectType.list_details_tao_bao_msg;
                break;
            case CollectType.list_details_1688:
                msg_0 = CollectType.list_details_1688_msg;
                break;
            default:
                msg_0 = "";
                break;
        }
        //替换占位符
        sendContent = sendContent.replace("#{0}", msg_0);
        sendContent = sendContent.replace("#{1}", msg_1);
        amqpServer.send(AMQPType.EMAIL_QUEUE, EmailParameterType.TARGET_EMAIL, targetEmail, EmailParameterType.CONTENT, sendContent);
    }

    /**
     * 创建以图搜货的淘宝产品详情信息
     *
     * @param userId
     * @param shopId
     * @param category
     * @param shopUrl
     * @param imgUrl
     */
    @RabbitListener(queues = AMQPType.LIST_DETAILS_TAO_BAO)
    // @Async("executor_fixed_4")
    //String userId, String shopId, CollectType category, String shopUrl, String imgUrl
    public void saveSearchImageShopUrl_tao_bao(Map<Object, Object> param) {
        boolean searchError = false;

        //参数
        String userId = String.valueOf(param.get("userId"));
        CollectType category = CollectType.valueOf(String.valueOf(param.get("category")));
        String shopId = String.valueOf(param.get("shopId"));
        String shopUrl = String.valueOf(param.get("shopUrl"));
        String imgUrl = String.valueOf(param.get("imgUrl"));

        //替换图片大小
        imgUrl = replaceImageSize(imgUrl, 400, 400);

        System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx>\n" + imgUrl);

        ShopDto shopDto = new ShopDto();
        shopDto.setId(UUID.randomUUID().toString());
        shopDto.setParentId(shopId);
        shopDto.setUserId(userId);
        shopDto.setOrderTime(new Date());
        long s = System.currentTimeMillis();
        WebDriver chromeDriver = null;
        String currentUrl = "";
        List<ComparisonInfoDto> taobao_item = null;
        try {
            //锁上
            lockUp_searchImg();
            chromeDriver = webDriverPool.getChromeDriver();
            chromeDriver.get(URLType.home_tao_bao);
            //下载图片到桌面
            if (imgUrl.contains("https:")) {
                DownLoadUtil.downLoadImg(imgUrl, DownLoadUtil.getDesktopPath() + "/1.jpg");
            } else {
                imgUrl = imgUrl.replace("http:", "");
                DownLoadUtil.downLoadImg("http:" + imgUrl, DownLoadUtil.getDesktopPath() + "/1.jpg");
            }
            //等待
            WebDriverWait wait_1 = new WebDriverWait(chromeDriver, 60);
            wait_1.until(ExpectedConditions.presenceOfElementLocated(By.className("drop-wrapper")));
            chromeDriver.findElement(By.className("drop-wrapper")).click();
            //搜图操作
            HttpClientUtil.searchImg();

            //解锁
            lockDown_searchImg();
            //确保搜图操作加载页面成功
            int size = 0;
            int maxSize = 2000;
            while (size < maxSize) {
                currentUrl = chromeDriver.getCurrentUrl();
                if (currentUrl.equals(URLType.home_tao_bao)) {
                    System.out.println("淘宝："+size);
                    size++;
                    continue;
                }
                System.out.println(currentUrl);
                break;
            }
            if (size >= maxSize)
                searchError = true;


        } catch (Exception e) {
            lockDown_searchImg();
            e.printStackTrace();
        } finally {
            //解锁
            //lockDown_searchImg();
        }

        //如果搜图超时出错 则报错 重新假如队列
        if (searchError) {
            CollectorService collectorService = null;
            System.out.println("如果搜图超时出错 则报错 重新假如队列");
            if (chromeDriver != null)
                webDriverPool.close(chromeDriver);
            collectorService.replaceImageSize("", 1, 1);
        }

        try {
            taobao_item = create_taobao_item_javaScript(chromeDriver, currentUrl, shopDto.getId(), userId);
        } finally {
            if (chromeDriver != null)
                //chromeDriver.quit();
                webDriverPool.close(chromeDriver);
        }
        //没搜到则不创建记录
        if (ListUtils.getMethods.isBlank(taobao_item))
            return;

        shopDto.setCategory(category.name());
        shopDto.setUrl(currentUrl);
        //保存搜图的记录
        if (!StringUtils.isBlank(currentUrl)) {
            jooqStorage.create(Shop.SHOP, ShopDto.class, shopDto);
            jooqStorage.batchCreate(Comparisoninfo.COMPARISONINFO, taobao_item);
        }
        System.out.println("saveSearchImageShopUrl_tao_bao 用时>>>" + (System.currentTimeMillis() - s));
    }

    /**
     * 创建以图搜货的1688产品详情信息
     *
     * @param userId
     * @param shopId
     * @param category
     * @param shopUrl
     * @param imgUrl
     */
    @RabbitListener(queues = AMQPType.LIST_DETAILS_1688)
    // @Async("executor_fixed_4")
    //String userId, String shopId, CollectType category, String shopUrl, String imgUrl
    public void saveSearchImageShopUrl_1688(Map<Object, Object> param) {
        //参数
        String userId = String.valueOf(param.get("userId"));
        CollectType category = CollectType.valueOf(String.valueOf(param.get("category")));
        String shopId = String.valueOf(param.get("shopId"));
        String shopUrl = String.valueOf(param.get("shopUrl"));
        String imgUrl = String.valueOf(param.get("imgUrl"));

        //替换图片大小
        imgUrl = replaceImageSize(imgUrl, 400, 400);

        ShopDto shopDto = new ShopDto();
        shopDto.setId(UUID.randomUUID().toString());
        shopDto.setParentId(shopId);
        shopDto.setUserId(userId);
        shopDto.setOrderTime(new Date());

        List<ComparisonInfoDto> taobao_item = null;
        long s = System.currentTimeMillis();
        WebDriver chromeDriver = null;
        String currentUrl = "";
        try {
            //锁上
            lockUp_searchImg();
            chromeDriver = webDriverPool.getChromeDriver();
            chromeDriver.get(URLType.home_1688);
            //下载图片到桌面
            if (imgUrl.contains("https:")) {
                DownLoadUtil.downLoadImg(imgUrl, DownLoadUtil.getDesktopPath() + "/1.jpg");
            } else {
                imgUrl = imgUrl.replace("http:", "");
                DownLoadUtil.downLoadImg("http:" + imgUrl, DownLoadUtil.getDesktopPath() + "/1.jpg");
            }
            //等待
            WebDriverWait wait_1 = new WebDriverWait(chromeDriver, 20);
            wait_1.until(ExpectedConditions.presenceOfElementLocated(By.className("sm-widget-picbtn")));
            //chromeDriver.findElement(By.className("identity-close-icon")).click();
            chromeDriver.findElement(By.className("sm-widget-picbtn")).click();
            //搜图操作
            HttpClientUtil.searchImg();

            //解锁
            lockDown_searchImg();
            //确保搜图操作加载页面成功
            int size = 0;
            while (size < 2000) {
                currentUrl = chromeDriver.getCurrentUrl();
                if (currentUrl.equals(URLType.detail_1688)) {
                    System.out.println("1688 ......：" + size);
                    size++;
                    continue;
                }
                System.out.println(currentUrl);
                break;
            }
            phantomJsUtil.getJquery(chromeDriver);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //解锁
            //lockDown_searchImg();
        }

        try {
            taobao_item = create_1688_item_javaScript(chromeDriver, currentUrl, shopDto.getId(), userId);
        } finally {
            if (chromeDriver != null)
                //chromeDriver.quit();
                webDriverPool.close(chromeDriver);
        }
        //没搜到则不创建记录
        if (ListUtils.getMethods.isBlank(taobao_item))
            return;

        shopDto.setCategory(category.name());
        shopDto.setUrl(currentUrl);
        //保存搜图的记录
        if (!StringUtils.isBlank(currentUrl)) {
            jooqStorage.create(Shop.SHOP, ShopDto.class, shopDto);
            jooqStorage.batchCreate(Comparisoninfo.COMPARISONINFO, taobao_item);
        }
        System.out.println("saveSearchImageShopUrl_1688 用时>>>" + (System.currentTimeMillis() - s));
    }

    /**
     * 直接js代码获取1688以图搜货的实体集合
     *
     * @param webDriver
     * @param downUrl
     * @param shopId
     * @param userId
     * @return
     */
    public List<ComparisonInfoDto> create_1688_item_javaScript(WebDriver webDriver, String downUrl, String shopId, String userId) {
        List<ComparisonInfoDto> comparisonInfoDtoLList = new ArrayList<>();
        try {
            // webDriver.get(downUrl);
            //滚动到底部
            phantomJsUtil.scrollTop(webDriver, 60);
            long s = System.currentTimeMillis();
            JavascriptExecutor webDriverJS = (JavascriptExecutor) webDriver;
            //价格
            String price = String.valueOf(webDriverJS.executeScript("var length=$('.sw-dpl-offer-item').length;var result='';for(var i=0;i<length;i++){result+=$('.sw-dpl-offer-item:eq('+i+') i').eq(1).attr('title')+';'} return result;"));
            //总成交数
            String dealCnt = String.valueOf(webDriverJS.executeScript("var length=$('.sw-dpl-offer-item').length;var result='';for(var i=0;i<length;i++){result+=$('.sw-dpl-offer-item:eq('+i+') .sw-dpl-offer-trade').attr('title')+';'} return result;"));
            //标题
            String title = String.valueOf(webDriverJS.executeScript("return $('.sw-dpl-offer-title a[title]').text()"));
            ////发货地
            String location = String.valueOf(webDriverJS.executeScript("return $('.sm-offer-sub').text()"));
            //公司名称
            String shop = String.valueOf(webDriverJS.executeScript("return $('.sw-dpl-offer-companyName').text()"));
            //主图
            String src = String.valueOf(webDriverJS.executeScript("var length=$('.sw-dpl-offer-photo img').length;var result='';for(var i=0;i<length;i++){result+=$('.sw-dpl-offer-photo img').eq(i).attr('src')+';'} return result;"));
            //商品链接
            String href = String.valueOf(webDriverJS.executeScript("var length=$('.sw-dpl-offer-item').length;var result='';for(var i=0;i<length;i++){result+=$('.sw-dpl-offer-item:eq('+i+') .sw-dpl-offer-photo a').attr('href')+';'} return result;"));
            //诚信通会员时间
            //TODO 暂时不需要
            String[] price_array = price.replaceAll("undefined", "?0").split(";");
            String[] dealCnt_array = dealCnt.replaceAll("undefined", "总成交0笔").split(";");
            String[] title_array = title.replaceAll("\\n\\t\\n\\t\\t", ";").replaceAll("\\n\\t\\t", "").replaceAll("\n\t", "").split(";");
            String[] location_array = location.replaceAll(" ", "").replaceAll("\n", "").replaceAll("\t\t\t\t\t", ";").replaceFirst(";", "").split(";");
            String[] shop_array = shop.replaceAll(" ", "").replaceAll("\\n\\n\\t\\n\\n\\t\\t", ";").replaceAll("\\n\\n\\t\\t", "").replaceAll("\\n\\n\\t", "").split(";");
            String[] src_array = src.split(";");
            String[] href_array = href.split(";");
            System.out.println("price_array >>" + price_array.length);
            System.out.println("dealCnt_array >>" + dealCnt_array.length);
            System.out.println("title_array >>" + title_array.length);
            System.out.println("location_array >>" + location_array.length);
            System.out.println("shop_array >>" + shop_array.length);
            System.out.println("src_array >>" + src_array.length);
            System.out.println("href_array >>" + href_array.length);

            for (int i = 0; i < price_array.length; i++) {
                ComparisonInfoDto comparisonInfoDto = new ComparisonInfoDto();
                comparisonInfoDto.setPrice(price_array[i]);
                comparisonInfoDto.setDealCnt(dealCnt_array[i]);
                comparisonInfoDto.setTitle(title_array[i]);
                comparisonInfoDto.setLocation(location_array[i]);
                comparisonInfoDto.setShop(shop_array[i]);
                comparisonInfoDto.setImg(src_array[i]);
                comparisonInfoDto.setUrl(href_array[i]);
                comparisonInfoDto.setShopId(shopId);
                comparisonInfoDto.setId(UUID.randomUUID().toString());
                comparisonInfoDto.setUserId(userId);
                comparisonInfoDtoLList.add(comparisonInfoDto);
            }

            System.out.println("create_1688_item_javaScript >>>" + (System.currentTimeMillis() - s));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("success>>>" + comparisonInfoDtoLList.size());
        return comparisonInfoDtoLList;
    }

    /**
     * 直接js代码获取淘宝以图搜货的实体集合
     *
     * @param webDriver
     * @param downUrl
     * @param shopId
     * @param userId
     * @return
     */
    public List<ComparisonInfoDto> create_taobao_item_javaScript(WebDriver webDriver, String downUrl, String shopId, String userId) {
        List<ComparisonInfoDto> comparisonInfoDtoLList = new ArrayList<>();
        try {
            //webDriver.get(downUrl);
            //滚动到底部
            phantomJsUtil.scrollTop(webDriver, 25);
            phantomJsUtil.getJquery(webDriver);
            long s = System.currentTimeMillis();
            JavascriptExecutor webDriverJS = (JavascriptExecutor) webDriver;
            //价格
            String price = String.valueOf(webDriverJS.executeScript("var length=40;var result='';for(var i=0;i<length;i++){result+=$('.item:eq('+i+') .price').text()+';'} return result;"));
            //总成交数
            String dealCnt = String.valueOf(webDriverJS.executeScript("var length=40;var result='';for(var i=0;i<length;i++){result+=$('.item:eq('+i+') .deal-cnt').text()+';'} return result;"));
            //标题
            String title = String.valueOf(webDriverJS.executeScript("var length=40;var result='';for(var i=0;i<length;i++){result+=$('.item:eq('+i+') .title').text()+';'} return result;"));
            ////发货地
            String location = String.valueOf(webDriverJS.executeScript("var length=40;var result='';for(var i=0;i<length;i++){result+=$('.item:eq('+i+') .location').text()+';'} return result;"));
            //公司名称
            String shop = String.valueOf(webDriverJS.executeScript("var length=40;var result='';for(var i=0;i<length;i++){result+=$('.item:eq('+i+') .shop').text()+';'} return result;"));
            //主图
            String src = String.valueOf(webDriverJS.executeScript("var length=40;var result='';for(var i=0;i<length;i++){result+=$('.item:eq('+i+') img').attr('src')+';'} return result;"));
            //商品链接
            String href = String.valueOf(webDriverJS.executeScript("var length=40;var result='';for(var i=0;i<length;i++){result+=$('.item:eq('+i+') .J_ItemPicA').attr('href')+';'} return result;"));
            //诚信通会员时间
            //TODO 暂时不需要
            String[] price_array = price.replaceAll(" ", "").replaceAll("\n", "").split(";");
            String[] dealCnt_array = dealCnt.split(";");
            String[] title_array = title.replaceAll("\n", "").replaceAll(" ", "").split(";");
            String[] location_array = location.split(";");
            String[] shop_array = shop.replaceAll(" ", "").replaceAll("\n", "").split(";");
            String[] src_array = src.split(";");
            String[] href_array = href.split(";");
            System.out.println("price_array >>" + price_array.length);
            System.out.println("dealCnt_array >>" + dealCnt_array.length);
            System.out.println("title_array >>" + title_array.length);
            System.out.println("location_array >>" + location_array.length);
            System.out.println("shop_array >>" + shop_array.length);
            System.out.println("src_array >>" + src_array.length);
            System.out.println("href_array >>" + href_array.length);

            List<Integer> maxLength = new ArrayList<>();
            maxLength.add(price_array.length);
            maxLength.add(dealCnt_array.length);
            maxLength.add(title_array.length);
            maxLength.add(location_array.length);
            maxLength.add(shop_array.length);
            maxLength.add(src_array.length);
            maxLength.add(href_array.length);
            System.out.println("采集可用的总条数>>>>>>" + Collections.min(maxLength));

            for (int i = 0; i < Collections.min(maxLength); i++) {
                ComparisonInfoDto comparisonInfoDto = new ComparisonInfoDto();
                comparisonInfoDto.setPrice(price_array[i]);
                comparisonInfoDto.setDealCnt(dealCnt_array[i]);
                comparisonInfoDto.setTitle(title_array[i]);
                comparisonInfoDto.setLocation(location_array[i]);
                comparisonInfoDto.setShop(shop_array[i]);
                comparisonInfoDto.setImg(src_array[i]);
                comparisonInfoDto.setUrl(href_array[i]);
                comparisonInfoDto.setShopId(shopId);
                comparisonInfoDto.setId(UUID.randomUUID().toString());
                comparisonInfoDto.setUserId(userId);
                comparisonInfoDtoLList.add(comparisonInfoDto);
            }

            System.out.println("create_tao_bao_item_javaScript >>>" + (System.currentTimeMillis() - s));
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("success>>>" + comparisonInfoDtoLList.size());
        return comparisonInfoDtoLList;
    }

    /**
     * 校验用户是否要email消息推送
     *
     * @param userId
     * @return
     */
    public boolean verifyEmailPush(String userId) {
        List<UserInfoDto> userInfoDtoList = jooqStorage.listByCondition(Userinfo.USERINFO, UserInfoDto.class, Userinfo.USERINFO.USERID.eq(userId).and(Userinfo.USERINFO.EMAIL_PUSH.eq(EmailPushType.on.name())));
        if (ListUtils.getMethods.isBlank(userInfoDtoList))
            return false;
        return true;
    }

    /**
     * 搜图之前其他线程不能执行方法
     */
    private void lockUp_searchImg() {
        while (searchImgFlag) {
            System.out.println("锁定中.........");
            continue;
        }
        synchronized (searchImgFlagLock) {
            searchImgFlag = true;
        }
    }

    /**
     * 搜图之后解锁线程
     */
    private void lockDown_searchImg() {
        synchronized (searchImgFlagLock) {
            searchImgFlag = false;
        }
    }

    /**
     * 获取字段列表
     *
     * @return
     */
    public BaseBean getFieldList() {
        List<FieldsDto> fieldsDtoList = jooqStorage.dslSelect(dsl -> dsl.select().from(Fields.FIELDS).orderBy(Fields.FIELDS.ORDER), FieldsDto.class);
        if (ListUtils.getMethods.isBlank(fieldsDtoList))
            return result("无字段列表");
        return result(fieldsDtoList);
    }

    protected <T> T getFieldValue(WebDriver webDriver, String executeJavascript) {
        JavascriptExecutor webDriverJS = (JavascriptExecutor) webDriver;
        System.out.println(executeJavascript);
        //String js2="var number=''; var length= $(\".obj-content img\").length; for(var i=0;i<length;i++){number+=$(\".obj-content img\")[i].src+';'} return number;";
        //String result = String.valueOf(webDriverJS.executeScript(js2));
        //System.out.println(result);
        return (T) String.valueOf(webDriverJS.executeScript(executeJavascript));
    }

    //替换图片大小
    private String replaceImageSize(String imageUrl, int width, int height) {
        String bigImage = imageUrl;
        String littleImage = imageUrl;
        String bigValue = String.valueOf(width) + "x" + String.valueOf(height);
        String littleValue = String.valueOf(width) + "x" + String.valueOf(height);

        for (int i = 0; i <= 800; i += 2) {
            String replaceValue = i + "x" + i;
            bigImage = bigImage.replace(replaceValue, bigValue);
            littleImage = littleImage.replace(replaceValue, littleValue);
        }
        return bigImage;
    }

}
