/*
 * This file is generated by jOOQ.
*/
package cn.xchats.onlinetraffic.collect.amqp.server.repository;


import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.Collect;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.Collectcategory;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.Comparisoninfo;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.Fields;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.Ip;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.Shop;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.User;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.Userinfo;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.records.CollectRecord;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.records.CollectcategoryRecord;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.records.ComparisoninfoRecord;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.records.FieldsRecord;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.records.IpRecord;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.records.ShopRecord;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.records.UserRecord;
import cn.xchats.onlinetraffic.collect.amqp.server.repository.tables.records.UserinfoRecord;

import javax.annotation.Generated;

import org.jooq.ForeignKey;
import org.jooq.UniqueKey;
import org.jooq.impl.Internal;


/**
 * A class modelling foreign key relationships and constraints of tables of 
 * the <code>online-traffic</code> schema.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------


    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<CollectRecord> KEY_COLLECT_PRIMARY = UniqueKeys0.KEY_COLLECT_PRIMARY;
    public static final UniqueKey<CollectcategoryRecord> KEY_COLLECTCATEGORY_PRIMARY = UniqueKeys0.KEY_COLLECTCATEGORY_PRIMARY;
    public static final UniqueKey<ComparisoninfoRecord> KEY_COMPARISONINFO_PRIMARY = UniqueKeys0.KEY_COMPARISONINFO_PRIMARY;
    public static final UniqueKey<FieldsRecord> KEY_FIELDS_PRIMARY = UniqueKeys0.KEY_FIELDS_PRIMARY;
    public static final UniqueKey<IpRecord> KEY_IP_PRIMARY = UniqueKeys0.KEY_IP_PRIMARY;
    public static final UniqueKey<ShopRecord> KEY_SHOP_PRIMARY = UniqueKeys0.KEY_SHOP_PRIMARY;
    public static final UniqueKey<UserRecord> KEY_USER_PRIMARY = UniqueKeys0.KEY_USER_PRIMARY;
    public static final UniqueKey<UserinfoRecord> KEY_USERINFO_PRIMARY = UniqueKeys0.KEY_USERINFO_PRIMARY;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------

    public static final ForeignKey<CollectRecord, ShopRecord> COLLECT_IBFK_1 = ForeignKeys0.COLLECT_IBFK_1;
    public static final ForeignKey<ComparisoninfoRecord, ShopRecord> COMPARISONINFO_IBFK_1 = ForeignKeys0.COMPARISONINFO_IBFK_1;
    public static final ForeignKey<IpRecord, UserinfoRecord> IP_IBFK_1 = ForeignKeys0.IP_IBFK_1;
    public static final ForeignKey<UserinfoRecord, UserRecord> USERINFO_IBFK_1 = ForeignKeys0.USERINFO_IBFK_1;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class UniqueKeys0 {
        public static final UniqueKey<CollectRecord> KEY_COLLECT_PRIMARY = Internal.createUniqueKey(Collect.COLLECT, "KEY_collect_PRIMARY", Collect.COLLECT.ID);
        public static final UniqueKey<CollectcategoryRecord> KEY_COLLECTCATEGORY_PRIMARY = Internal.createUniqueKey(Collectcategory.COLLECTCATEGORY, "KEY_collectcategory_PRIMARY", Collectcategory.COLLECTCATEGORY.ID);
        public static final UniqueKey<ComparisoninfoRecord> KEY_COMPARISONINFO_PRIMARY = Internal.createUniqueKey(Comparisoninfo.COMPARISONINFO, "KEY_comparisoninfo_PRIMARY", Comparisoninfo.COMPARISONINFO.ID);
        public static final UniqueKey<FieldsRecord> KEY_FIELDS_PRIMARY = Internal.createUniqueKey(Fields.FIELDS, "KEY_fields_PRIMARY", Fields.FIELDS.ID);
        public static final UniqueKey<IpRecord> KEY_IP_PRIMARY = Internal.createUniqueKey(Ip.IP, "KEY_ip_PRIMARY", Ip.IP.ID);
        public static final UniqueKey<ShopRecord> KEY_SHOP_PRIMARY = Internal.createUniqueKey(Shop.SHOP, "KEY_shop_PRIMARY", Shop.SHOP.ID);
        public static final UniqueKey<UserRecord> KEY_USER_PRIMARY = Internal.createUniqueKey(User.USER, "KEY_user_PRIMARY", User.USER.ID);
        public static final UniqueKey<UserinfoRecord> KEY_USERINFO_PRIMARY = Internal.createUniqueKey(Userinfo.USERINFO, "KEY_userinfo_PRIMARY", Userinfo.USERINFO.ID);
    }

    private static class ForeignKeys0 {
        public static final ForeignKey<CollectRecord, ShopRecord> COLLECT_IBFK_1 = Internal.createForeignKey(cn.xchats.onlinetraffic.collect.amqp.server.repository.Keys.KEY_SHOP_PRIMARY, Collect.COLLECT, "collect_ibfk_1", Collect.COLLECT.SHOPID);
        public static final ForeignKey<ComparisoninfoRecord, ShopRecord> COMPARISONINFO_IBFK_1 = Internal.createForeignKey(cn.xchats.onlinetraffic.collect.amqp.server.repository.Keys.KEY_SHOP_PRIMARY, Comparisoninfo.COMPARISONINFO, "comparisoninfo_ibfk_1", Comparisoninfo.COMPARISONINFO.SHOPID);
        public static final ForeignKey<IpRecord, UserinfoRecord> IP_IBFK_1 = Internal.createForeignKey(cn.xchats.onlinetraffic.collect.amqp.server.repository.Keys.KEY_USERINFO_PRIMARY, Ip.IP, "ip_ibfk_1", Ip.IP.USERINFOID);
        public static final ForeignKey<UserinfoRecord, UserRecord> USERINFO_IBFK_1 = Internal.createForeignKey(cn.xchats.onlinetraffic.collect.amqp.server.repository.Keys.KEY_USER_PRIMARY, Userinfo.USERINFO, "userinfo_ibfk_1", Userinfo.USERINFO.USERID);
    }
}
