package cn.xchats.onlinetraffic.collect.amqp.server.config;

import cn.xchats.onlinetraffic.common.type.AMQPType;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/*
 *
 *@author teddy
 *@date 2018/5/7
 */
@Component
public class QueueConfig {

    @Bean
    public Queue queue_1(){
        return new Queue(AMQPType.LIST_HOME_1688);
    }
    @Bean
    public Queue queue_2(){
        return new Queue(AMQPType.LIST_HOME_TAO_BAO);
    }
    @Bean
    public Queue queue_3(){
        return new Queue(AMQPType.LIST_DETAILS_1688);
    }
    @Bean
    public Queue queue_4(){
        return new Queue(AMQPType.LIST_DETAILS_TAO_BAO);
    }
    @Bean
    public Queue queue_5(){
        return new Queue(AMQPType.LIST_HOME);
    }

}
