package cn.xchats.onlinetraffic.collect.amqp.server.util;

import cn.xchats.onlinetraffic.common.other.http.httpclient.HttpClientUtil;
import cn.xchats.onlinetraffic.common.other.util.PropertyUtil;
import cn.xchats.onlinetraffic.common.other.webdriver.PhantomJsUtil;
import cn.xchats.onlinetraffic.common.type.URLType;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/*
 *
 *@author teddy
 *@date 2018/5/7
 */
@Service
@SuppressWarnings("all")
public class WebDriverPool {
    private int phantomjsDriverPoolSize = Integer.valueOf(PropertyUtil.getProperty("webDriver.pool.phantomjs.size"));//池子大小
    private int chromeDriverPoolSize = Integer.valueOf(PropertyUtil.getProperty("webDriver.pool.chrome.size"));//池子大小
    private boolean adjustmentSize = Boolean.valueOf(PropertyUtil.getProperty("run.environment.adjustment.size"));//是否要在启动的时候调整浏览器大小

    private List<WebDriver> phantomjsDriverPool = new ArrayList<>(phantomjsDriverPoolSize);
    private List<WebDriver> chromeDriverPool = new ArrayList<>(chromeDriverPoolSize);

    private List<Integer> select_phantomjs = new LinkedList<>(); //0要阻塞
    private List<Integer> select_chrome = new LinkedList<>();//0要阻塞

    private PhantomJsUtil phantomJsUtil = new PhantomJsUtil();

    public WebDriverPool() {
        init();
    }

    /*public WebDriverPool(int phantomjsDriverPoolSize,int chromeDriverPoolSize) {
        this.phantomjsDriverPoolSize = phantomjsDriverPoolSize;
        this.chromeDriverPoolSize = chromeDriverPoolSize;
    }*/


    public void init() {
        //初始化awt项目
        //HttpClientUtil.searchInit();
        //初始化phantomjsDriverPool
        for (int i = 0; i < phantomjsDriverPoolSize; i++) {
            phantomjsDriverPool.add(phantomJsUtil.getPhantomJs());
            phantomjsDriverPool.get(i).get(URLType.home_1688);
            select_phantomjs.add(i);
        }
        System.err.println("phantomjsDriverPool  init  success ..!  size>>>" + phantomjsDriverPool.size());
        //初始化chromeDriverPool
        for (int i = 0; i < chromeDriverPoolSize; i++) {
            chromeDriverPool.add(phantomJsUtil.getChromeDriver());
            chromeDriverPool.get(i).get("https://detail.1688.com");
            WebDriverWait wait_1 = new WebDriverWait(chromeDriverPool.get(i), 20);
            wait_1.until(ExpectedConditions.presenceOfElementLocated(By.className("sm-widget-picbtn")));
            chromeDriverPool.get(i).findElement(By.className("identity-close-icon")).click();
            //chromeDriverPool.get(i).findElement(By.className("sm-widget-picbtn")).click();
            select_chrome.add(i);
        }
        System.err.println("chromeDriverPool  init  success ..!  size>>>" + chromeDriverPool.size());

        //确认浏览器调整完成
        if (!adjustmentSize)
            return;
        Scanner sc = new Scanner(System.in);
        String confirm="y";
        while (true){
            System.out.println("请确认浏览器大小是否调整完毕?(y/n)");
            String value = sc.nextLine();  //读取字符串型输入
            if (confirm.equals(value)){
                System.out.println("确认完毕,项目正在启动");
                return;
            }
        }
    }

    //选取一个可用的webdriver
    public synchronized int select_phantomjs() {
        while (true) {
            if (select_phantomjs.size() != 0) {
                int result = select_phantomjs.get(0);//每次选取第一个
                select_phantomjs.remove(0);
                return result;
            }
        }
    }

    public synchronized int select_chrome() {
        while (true) {
            if (select_chrome.size() != 0) {
                int result = select_chrome.get(0);//每次选取第一个
                select_chrome.remove(0);
                return result;
            }
        }
    }

    public synchronized void close(WebDriver webDriver) {
        if (webDriver instanceof ChromeDriver) {
            for (int i = 0; i < chromeDriverPool.size(); i++) {
                if (webDriver == chromeDriverPool.get(i)) {
                    select_chrome.add(i);
                    break;
                }
            }
        } else {
            for (int i = 0; i < phantomjsDriverPool.size(); i++) {
                if (webDriver == phantomjsDriverPool.get(i)) {
                    select_phantomjs.add(i);
                    break;
                }
            }
        }
    }

    public WebDriver getChromeDriver() {
        WebDriver webDriver = chromeDriverPool.get(select_chrome());
        return webDriver;
    }

    public WebDriver getPhantomJs() {
        WebDriver webDriver = phantomjsDriverPool.get(select_phantomjs());
        return webDriver;
    }

}

