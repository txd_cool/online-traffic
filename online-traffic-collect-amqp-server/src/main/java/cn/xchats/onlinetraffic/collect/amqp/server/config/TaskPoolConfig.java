package cn.xchats.onlinetraffic.collect.amqp.server.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/*
 *
 *@author teddy
 *@date 2018/4/20
 */
@EnableAsync
@Configuration
public class TaskPoolConfig {

    @Bean("executor_fixed_1")
    public Executor executor_fixed_1() {
        return Executors.newFixedThreadPool(1);
    }
    @Bean("executor_fixed_2")
    public Executor executor_fixed_2() {
        return Executors.newFixedThreadPool(2);
    }
    @Bean("executor_fixed_3")
    public Executor executor_fixed_3() {
        return Executors.newFixedThreadPool(3);
    }
    @Bean("executor_fixed_4")
    public Executor executor_fixed_4() {
        return Executors.newFixedThreadPool(4);
    }
    @Bean("executor_fixed_5")
    public Executor executor_fixed_5() {
        return Executors.newFixedThreadPool(5);
    }

}
