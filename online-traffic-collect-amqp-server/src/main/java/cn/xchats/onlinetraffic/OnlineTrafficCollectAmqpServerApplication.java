package cn.xchats.onlinetraffic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineTrafficCollectAmqpServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineTrafficCollectAmqpServerApplication.class, args);
	}
}
