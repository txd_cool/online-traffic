package cn.xchats.onlinetraffic.collect.amqp.server;

import cn.xchats.onlinetraffic.OnlineTrafficCollectAmqpServerApplication;
import cn.xchats.onlinetraffic.collect.amqp.server.util.WebDriverPool;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OnlineTrafficCollectAmqpServerApplication.class)
public class OnlineTrafficCollectAmqpServerApplicationTests {

	@Autowired
	private WebDriverPool webDriverPool;

	@Test
	public void contextLoads() {
	}

	@Test
	public void test_1(){
		List<String> urlList=new ArrayList<>();
		urlList.add("https://mp.weixin.qq.com/s/VyAK1bIWDw7pCIxtwxVZmQ");
		urlList.add("https://mp.weixin.qq.com/s/fN3-U52aFUx4jnu15uRKeA");
		urlList.add("https://mp.weixin.qq.com/s/rkzgHZeIFsAMBwGWjHKVPw");
		urlList.add("https://mp.weixin.qq.com/s/ekrcieqVpfyZvz4DdrxDKg");
		urlList.add("https://mp.weixin.qq.com/s/CPGDBMUBr27RUuMfFRWIRg");
		urlList.add("https://mp.weixin.qq.com/s/ullfa6AebH8FWowQWwAeZg");
		urlList.add("https://mp.weixin.qq.com/s/zp_6jkMadAM6WgTXU0oQkA");
		urlList.add("https://mp.weixin.qq.com/s/e48TqefCLjJspoBJiQrqbA");
		urlList.add("https://mp.weixin.qq.com/s/ytaPXAIcjBIqVlolSWcBHg");
		urlList.add("https://mp.weixin.qq.com/s/Dt6JstttYt0J5wfFw7U50w");
		urlList.add("https://mp.weixin.qq.com/s/yD75ekvjUf6AOCcPwEHLHw");
		urlList.add("https://mp.weixin.qq.com/s/S5TZ3uS_j4_DsGYrp9zKRw");
		urlList.add("https://mp.weixin.qq.com/s/e7PUcmfUW0iAq0wCVP2Ghg");
		urlList.add("https://mp.weixin.qq.com/s/NnZfU-N-oDfjJ5aUFbJOPQ");
		urlList.add("https://mp.weixin.qq.com/s/ftKcnvpARXa1P9ZQfKuO1A");
		WebDriver webDriver = webDriverPool.getPhantomJs();
		for (int i=0;i<urlList.size();i++){
			webDriver.get(urlList.get(i));
			JavascriptExecutor webDriverJS = (JavascriptExecutor) webDriver;
			//时间
			Object time = webDriverJS.executeScript("return publish_time");
			//标题
			String title = webDriver.findElement(By.id("activity-name")).getText();
			//原创
			String original = webDriver.findElement(By.id("copyright_logo")).getText();

			System.out.println("时间:  "+ time);
			System.out.println("标题:  "+ title);
			System.out.println("原创:  "+ original);
			System.out.println("====================================================");
		}

		webDriverPool.close(webDriver);
		System.out.println("exit");
	}
}
