package cn.xchats.onlinetraffic.jooq.frame.common;

import java.io.Serializable;

/*
 *接口返回类型Bean
 *@author teddy
 *@date 2018/1/30
 */
public class MessageBean<T> extends BaseBean implements Serializable {

    private static final long serialVersionUID = 1L;

    private String userInfoId;
    private String operation;//添加,删除,修改
    private T data;

    public MessageBean() {
    }

    public MessageBean(String meta, T data, String userInfoId, String operation) {
        this.userInfoId = userInfoId;
        this.operation = operation;
        this.meta = meta;
        this.data = data;
    }

    public String getUserInfoId() {
        return userInfoId;
    }

    public void setUserInfoId(String userInfoId) {
        this.userInfoId = userInfoId;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
