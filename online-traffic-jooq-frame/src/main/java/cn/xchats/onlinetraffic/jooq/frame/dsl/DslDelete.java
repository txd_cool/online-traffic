package cn.xchats.onlinetraffic.jooq.frame.dsl;

import org.jooq.DSLContext;
import org.jooq.Delete;
import org.jooq.Record;

/**
 * 
 * <p>@author txd.cool@gmail.com</p>
 * <p>2017年5月13日</p>
 * <p>version 1.0</p>
 */
@FunctionalInterface
public interface DslDelete<R extends Record> {
	Delete<R> delete(DSLContext var1);
}
