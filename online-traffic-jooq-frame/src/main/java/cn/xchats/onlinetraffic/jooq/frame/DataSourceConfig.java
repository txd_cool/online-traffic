package cn.xchats.onlinetraffic.jooq.frame;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.jooq.impl.DefaultConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Created by TangGod on 2024/7/31.
 *
 * https://jooq.diamondfsd.com/learn/section-7-multiple-datasource
 * https://cloud.tencent.cn/developer/information/%E5%9C%A8spring%20boot%E5%BA%94%E7%94%A8%E7%A8%8B%E5%BA%8F%E4%B8%AD%E4%BD%BF%E7%94%A8jooq%E8%BF%9E%E6%8E%A5%E5%88%B0%E5%A4%9A%E4%B8%AAMySQL%E6%95%B0%E6%8D%AE%E5%BA%93%E5%AE%9E%E4%BE%8B
 */
@Configuration
public class DataSourceConfig {

    @Primary
    @Bean(name = "dsDefault")
    @ConfigurationProperties(prefix = "spring.datasource.default")
    public DataSource dataSource1() {
        return DataSourceBuilder.create().build();
    }

    @Primary
    @Bean(name = "dslContextDefault")
    public DSLContext dslContext1(@Qualifier("dsDefault") DataSource ds1) {
        return DSL.using(ds1, SQLDialect.MYSQL);
    }
}
