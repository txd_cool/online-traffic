package cn.xchats.onlinetraffic.jooq.frame.dsl;


import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Select;

/**
 * 
 * <p>@author txd.cool@gmail.com</p>
 * <p>2017年5月13日</p>
 * <p>version 1.0</p>
 */
@FunctionalInterface
public interface DslSelect<R extends Record> {
	Select<R> select(DSLContext var1);
}
