package cn.xchats.onlinetraffic.jooq.frame.dsl;

import org.jooq.DSLContext;
import org.jooq.Insert;
import org.jooq.Record;

/**
 * 
 * <p>@author txd.cool@gmail.com</p>
 * <p>2017年5月13日</p>
 * <p>version 1.0</p>
 */
@FunctionalInterface
public interface DslInsert<R extends Record> {
	Insert<R> dslInsert(DSLContext var1);
}
