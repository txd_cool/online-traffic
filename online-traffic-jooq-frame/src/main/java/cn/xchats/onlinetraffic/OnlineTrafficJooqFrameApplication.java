package cn.xchats.onlinetraffic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineTrafficJooqFrameApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineTrafficJooqFrameApplication.class, args);
	}
}
