package dto;

import lombok.Data;

/*
 *
 *@author teddy
 *@date 2018/5/22
 */
@Data
public class ResponseResult {

    private String url;
    private String name;
}
