package cn.xchats.onlinetraffic.other.amqp.server;

import cn.xchats.onlinetraffic.common.other.mq.AMQPServer;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/*
 *
 *@author teddy
 *@date 2018/5/16
 */
@Component
@RabbitListener(bindings = @QueueBinding(
        value =@Queue(value = "${mq.config.queue.info}",autoDelete = "true"),
        exchange = @Exchange(value = "${mq.config.exchange}",type = ExchangeTypes.DIRECT),
        key = "${mq.config.queue.info.routing.key}"
))
/*@RabbitListener(bindings = @QueueBinding(
        value =@Queue(value = "log.info",autoDelete = "true"),
        exchange = @Exchange(value = "log.direct",type = ExchangeTypes.DIRECT),
        key = "log.info.routing.key"
))*/
public class InfoReceiver {

    @Autowired
    private static AMQPServer amqpServer;


    public static void test1(){
        amqpServer.send("log.direct","log.info.routing.key","我是log。。。");
    }

    @RabbitHandler
    public void log(String msg){
        System.out.println("log >>"+msg);
    }
}
