package cn.xchats.onlinetraffic.other.amqp.server;

import cn.xchats.onlinetraffic.common.other.http.java.DownLoadUtil;
import cn.xchats.onlinetraffic.common.type.AMQPType;
import cn.xchats.onlinetraffic.common.type.EmailParameterType;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.util.Map;
import java.util.Properties;

/*
 *
 *@author teddy
 *@date 2018/5/16
 */
@Service
public class MailService {

    @Autowired
    private JavaMailSender jms;

    @Autowired
    JavaMailSenderImpl senderImpl;

    //建立邮件消息
    private SimpleMailMessage mainMessage = new SimpleMailMessage();

    public MailService() {
        //发送者
        //mainMessage.setFrom("Hang_Zhou_Jiu_Ding@163.com");
        mainMessage.setFrom("1084986263@qq.com");
        //发送的标题
        mainMessage.setSubject("线上买卖通:任务完成通知");
    }

    @Bean
    public Queue queue() {
        return new Queue(AMQPType.EMAIL_QUEUE);
    }

    //@RabbitListener(queues = AMQPType.EMAIL_QUEUE)
    public void send(Map<Object, Object> param) {
        String targetEmail, content;
        targetEmail = String.valueOf(param.get(EmailParameterType.TARGET_EMAIL));
        content = String.valueOf(param.get(EmailParameterType.CONTENT));
        //接收者
        mainMessage.setTo(targetEmail);
        //发送的内容
        mainMessage.setText(content);
        try {
            jms.send(mainMessage);
        } catch (MailException e) {
            e.printStackTrace();
            System.out.println("发送失败,邮箱不正确");
            return;
        }
        System.out.println("发送成功>> :" + targetEmail);
    }

    @RabbitListener(queues = AMQPType.EMAIL_QUEUE)
    public void sendHtmlMail(Map<Object, Object> param) {
        MimeMessage message = null;
        String targetEmail, content;
        targetEmail = String.valueOf(param.get(EmailParameterType.TARGET_EMAIL));
        content = String.valueOf(param.get(EmailParameterType.CONTENT));
        try {
            message = jms.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            helper.setTo(targetEmail);

            //发送者
            helper.setFrom("1084986263@qq.com");
            //发送的标题
            helper.setSubject("线上买卖通:任务完成通知");

            StringBuffer sb = new StringBuffer();
            sb.append("<h1></h1>")
                    .append("<p style='color:#F00'>123</p>")
                    .append("<img src='cid:image' />");

            helper.setText(sb.toString(), true);

            FileSystemResource img = new FileSystemResource(new File("D:\\1.png"));
            helper.addInline("image",new File("D:\\1.png"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        jms.send(message);
    }

}
