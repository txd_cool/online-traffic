package cn.xchats.onlinetrafficotheramqpserver;

import cn.xchats.onlinetraffic.OnlineTrafficOtherAmqpServerApplication;
import cn.xchats.onlinetraffic.common.other.mq.AMQPServer;
import cn.xchats.onlinetraffic.common.type.AMQPType;
import cn.xchats.onlinetraffic.common.type.EmailParameterType;
import cn.xchats.onlinetraffic.other.amqp.server.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = OnlineTrafficOtherAmqpServerApplication.class)
public class OnlineTrafficOtherAmqpServerApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Autowired
	private AMQPServer amqpServer;

	@Autowired
	private MailService mailService;

	@Test
	public void test(){
		String content="您有一个淘宝&天猫主页任务已完成：鞋子女2018新款春季凉鞋女夏一字扣中跟小清新高跟鞋女细跟猫跟鞋";//您有一个淘宝&天猫主页任务已完成：鞋子女2018新款春季凉鞋女夏一字扣中跟小清新高跟鞋女细跟猫跟鞋
		amqpServer.send(AMQPType.EMAIL_QUEUE, EmailParameterType.TARGET_EMAIL,"Hang_Zhou_Jiu_Ding@163.com",EmailParameterType.CONTENT,content);
	}

	@Test
	public void test_1(){
		mailService.sendHtmlMail(null);
	}
}
