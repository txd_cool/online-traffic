package cn.xchats.onlinetraffic.client.monitoring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineTrafficClientMonitoringApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineTrafficClientMonitoringApplication.class, args);
	}
}
