package cn.xchats.onlinetraffic.client.monitoring.config;

import de.codecentric.boot.admin.client.config.ClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/*
 *
 *@author teddy
 *@date 2018/6/8
 */
@Configuration
public class SecurityPermitAllConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().anyRequest().permitAll()
                .and().csrf().disable();

        //解决这个会导致使用iframe模式上传图片或者iframe嵌套页面时，会报如下异常信息：
        //Refused to display in a frame because it set ‘X-Frame-Options’ to ‘DENY’
        http.headers().frameOptions().disable();
    }


}
