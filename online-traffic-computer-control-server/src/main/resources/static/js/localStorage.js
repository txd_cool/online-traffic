//var serverURL='http://192.168.0.110:5000';//  http://txdcool.ittun/
var serverURL='http://online-traffic-data-server.ittun.com';
//var serverURL='http://47.95.249.239:5200';
var imageServerURL='http://47.95.249.239:5200';
//设置本地存储对象的值
function setLocalObject(key, value) {
    return localStorage.setItem(key, JSON.stringify(value));
}

function replaceAllString(split1, split2, value) {
    var length = value.split(split1).length;
    for (var i = 0; i < length; i++) {
        value = value.replace(split1, split2);
    }
    return value;
}

//获取本地存储对象的值
function getLocalObject(key) {
    return JSON.parse(localStorage.getItem(key));
}

//拼接元素
function getJointElement() {
    var element = new Array();
    //js中有个变量arguments,可以访问所有传入的值
    for (var i = 0; i < arguments.length; i++) {
        element.push(arguments[i]);
    }
    return element.join("");
}

function writeObj(obj) {
    var description = "";
    for (var i in obj) {
        var property = obj[i];
        description += i + " = " + property + "\n";
    }
    console.log(description);
}

function reloadList(messVal) {
    var message = "<div style='margin-left:100px;'>" + messVal + "</div>";
    layer.open({
        time: 2500,
        content: message,
        shade: 0,
        type: 7,
        area: ['300px', '80px'],
        offset: 't'
    });
}

function isNotBlank(value) {
    if (value == null || value == '' || value == undefined) {
        return false;
    }
    return true;
}

function isBlank(value) {
    if (value == null || value == '' || value == undefined) {
        return true;
    }
    return false;
}

function addAuthCode(loginName) {
    $.ajax({
        type: 'POST',
        url: './login/addAuthCode.do',
        data: {
            loginName: loginName
        },
        success: function (data) {
            localStorage.authCode = data;
        },
        error: function (e) {
            console.log(e)
        }
    });
}

function removeAuthCode(loginName) {
    $.ajax({
        type: 'POST',
        url: './login/removeAuthCode.do',
        data: {
            loginName: loginName
        },
        success: function (data) {
            localStorage.authCode = '';
        },
        error: function (e) {
            console.log(e)
        }
    });
}

//以下是本地存储key的注释信息,加了key请在下方增加注释
//localStorage.userEntityInfoId  // 用户id号
//localStorage.guardianUid // 监护人id号
//localStorage.count // 分页：总记录数
//localStorage.limit //分页：页面大小
//localStorage.realName // 监护人登录后、查询栏需要查询的字段
//localStorage.messageSuccess  //操作按钮点击时设置为true,reloadList(messVal) 提示方法使用
//localStorage.doctorGuardianAccount //医生监护人登陆保存的cookie账号
//localStorage.doctorGuardianPassword //医生监护人登陆保存的cookie密码
//localStorage.onlineStatus //监护人列表中管理的,用户当前在线状态
//localStorage.ecgDataMarkList//当前record数据的值
//localStorage.access //当前账号使用权限 : -1.用户 : 0 -2.家庭监护人: 1 -3.医生监护人 : 2

