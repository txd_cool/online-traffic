package cn.xchats.onlinetraffic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineTrafficComputerControlServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineTrafficComputerControlServerApplication.class, args);
	}
}
