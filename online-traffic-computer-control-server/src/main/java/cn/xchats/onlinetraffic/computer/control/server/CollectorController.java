package cn.xchats.onlinetraffic.computer.control.server;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 *
 *@author teddy
 *@date 2018/4/16
 */
@Controller
public class CollectorController {

    @RequestMapping("/search")
    @ResponseBody
    public String testSearch() {
        new AWTService().searchImg();
        return "任务已开始执行,请稍后..!";
    }

    @RequestMapping("/init")
    @ResponseBody
    public String init() {
        new AWTService().init();
        return "任务已开始执行,请稍后..!";
    }
}
