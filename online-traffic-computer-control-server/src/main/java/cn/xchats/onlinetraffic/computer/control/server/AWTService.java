package cn.xchats.onlinetraffic.computer.control.server;

import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;

/*
 *
 *@author teddy
 *@date 2018/4/18
 */
@Service
public class AWTService {

    private static int select;

    //执行搜图
    public void searchImg() {
        try {
            System.setProperty("java.awt.headless", "false");
            Robot robot = new Robot();
            //设置Robot产生一个动作后的休眠时间,否则执行过快
            //robot.setAutoDelay(1000);
         /*   //获取屏幕分辨率
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            System.out.println(d);
            //以屏幕的尺寸创建个矩形
            Rectangle screenRect = new Rectangle(d);
            //截图（截取整个屏幕图片）
            BufferedImage bufferedImage = robot.createScreenCapture(screenRect);
            //保存截图
            File file = new File("d:/screenRect.png");
            ImageIO.write(bufferedImage, "png", file);*/
            //移动鼠标
           /* robot.mouseMove(1140, 140);
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            Thread.sleep(3_000);*/
           if (select%2==0){
               robot.mouseMove(300, 415);
           }else{
               robot.mouseMove(1056, 415);
           }
            select++;
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            Thread.sleep(5_00);
            //键盘事件
            robot.keyPress(KeyEvent.VK_1);
            robot.keyPress(KeyEvent.VK_PERIOD);
            robot.keyPress(KeyEvent.VK_J);
            robot.keyPress(KeyEvent.VK_P);
            robot.keyPress(KeyEvent.VK_G);
            Thread.sleep(1_000);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //执行截图
    public void screenshot(String savePath) {
        try {
            Robot robot = new Robot();
            //以屏幕的尺寸创建个矩形
            Rectangle screenRect= new Rectangle(600, 240, 540, 270);
            //截图（截取整个屏幕图片）
            BufferedImage bufferedImage = robot.createScreenCapture(screenRect);
            //保存截图
            File file = new File(savePath);
            ImageIO.write(bufferedImage, "png", file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init(){
        this.select=0;
    }

    public static void main(String[] args) {
        AWTService awtService = new AWTService();
        //awtService.screenshot("d:/screenRect.png");
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        awtService.searchImg();
        //awtService.searchImg();
    }
}
