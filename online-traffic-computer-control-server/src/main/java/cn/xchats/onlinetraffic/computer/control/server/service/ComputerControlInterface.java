package cn.xchats.onlinetraffic.computer.control.server.service;

import cn.xchats.onlinetraffic.common.type.Event;

import java.awt.*;
import java.awt.event.KeyEvent;

/*
 *
 *@author teddy
 *@date 2018/5/29
 */
public interface ComputerControlInterface {


    /**
     * 选择一个事件
     *
     * @param eventKey
     * @return
     */
    default int selectEvent(String eventKey) {
        switch (eventKey) {
            //0 ~ 9
            case Event._0:
                return KeyEvent.VK_0;
            case Event._1:
                return KeyEvent.VK_1;
            case Event._2:
                return KeyEvent.VK_2;
            case Event._3:
                return KeyEvent.VK_3;
            case Event._4:
                return KeyEvent.VK_4;
            case Event._5:
                return KeyEvent.VK_5;
            case Event._6:
                return KeyEvent.VK_6;
            case Event._7:
                return KeyEvent.VK_7;
            case Event._8:
                return KeyEvent.VK_8;
            case Event._9:
                return KeyEvent.VK_9;
            //A ~ Z
            case Event._A:
                return KeyEvent.VK_A;
            case Event._B:
                return KeyEvent.VK_B;
            case Event._C:
                return KeyEvent.VK_C;
            case Event._D:
                return KeyEvent.VK_D;
            case Event._E:
                return KeyEvent.VK_E;
            case Event._F:
                return KeyEvent.VK_F;
            case Event._G:
                return KeyEvent.VK_G;
            case Event._H:
                return KeyEvent.VK_H;
            case Event._I:
                return KeyEvent.VK_I;
            case Event._J:
                return KeyEvent.VK_J;
            case Event._K:
                return KeyEvent.VK_K;
            case Event._L:
                return KeyEvent.VK_L;
            case Event._M:
                return KeyEvent.VK_M;
            case Event._N:
                return KeyEvent.VK_N;
            case Event._O:
                return KeyEvent.VK_O;
            case Event._P:
                return KeyEvent.VK_P;
            case Event._Q:
                return KeyEvent.VK_Q;
            case Event._R:
                return KeyEvent.VK_R;
            case Event._S:
                return KeyEvent.VK_S;
            case Event._T:
                return KeyEvent.VK_T;
            case Event._U:
                return KeyEvent.VK_U;
            case Event._V:
                return KeyEvent.VK_V;
            case Event._W:
                return KeyEvent.VK_W;
            case Event._X:
                return KeyEvent.VK_X;
            case Event._Y:
                return KeyEvent.VK_Y;
            case Event._Z:
                return KeyEvent.VK_Z;
            //F1 ~ F12
            case Event._F1:
                return KeyEvent.VK_F1;
            case Event._F2:
                return KeyEvent.VK_F2;
            case Event._F3:
                return KeyEvent.VK_F3;
            case Event._F4:
                return KeyEvent.VK_F4;
            case Event._F5:
                return KeyEvent.VK_F5;
            case Event._F6:
                return KeyEvent.VK_F6;
            case Event._F7:
                return KeyEvent.VK_F7;
            case Event._F8:
                return KeyEvent.VK_F8;
            case Event._F9:
                return KeyEvent.VK_F9;
            case Event._F10:
                return KeyEvent.VK_F10;
            case Event._F11:
                return KeyEvent.VK_F11;
            case Event._F12:
                return KeyEvent.VK_F12;

            //other
            case Event._HOME:
                return KeyEvent.VK_HOME;
            case Event._END:
                return KeyEvent.VK_END;
            case Event._PGUP:
                return KeyEvent.VK_PAGE_UP;
            case Event._PGDN:
                return KeyEvent.VK_PAGE_DOWN;
            case Event._UP:
                return KeyEvent.VK_UP;
            case Event._DOWN:
                return KeyEvent.VK_DOWN;
            case Event._LEFT:
                return KeyEvent.VK_LEFT;
            case Event._RIGHT:
                return KeyEvent.VK_RIGHT;
            case Event._ESCAPE:
                return KeyEvent.VK_ESCAPE;
            case Event._TAB:
                return KeyEvent.VK_TAB;
            case Event._CONTROL:
                return KeyEvent.VK_CONTROL;
            case Event._SHIFT:
                return KeyEvent.VK_SHIFT;
            case Event._BACK_SPACE:
                return KeyEvent.VK_SPACE;
            case Event._CAPS_LOCK:
                return KeyEvent.VK_CAPS_LOCK;
            case Event._NUM_LOCK:
                return KeyEvent.VK_NUM_LOCK;
            case Event._ENTER:
                return KeyEvent.VK_ENTER;
        }
        return -1;
    }

    /**
     * 创建一个实例
     * @return
     */
    default Robot newRobot( ){
        try {
            System.setProperty("java.awt.headless", "false");
            return new Robot();
        } catch (AWTException e) {
            System.out.println("robot >> 初始化出错...!");
        }
        return null;
    }

    /**
     * 执行某个事件
     * @param computer
     * @param event
     */
    default void executeEventInterface(Robot computer,String event){
        computer.keyPress(selectEvent(event));
    }
}
