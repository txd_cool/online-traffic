package cn.xchats.onlinetraffic.computer.control.server.rest;

import cn.xchats.onlinetraffic.common.dto.BaseBean;
import cn.xchats.onlinetraffic.common.dto.MessageBean;
import cn.xchats.onlinetraffic.common.type.Event;
import cn.xchats.onlinetraffic.computer.control.server.service.ComputerControlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/*
 *
 *@author teddy
 *@date 2018/5/29
 */
@Controller
public class ComputerController {

    @Autowired
    private ComputerControlService computerControlService;

    @RequestMapping("/")
    public String index(){
        return "config";
    }

    @RequestMapping("/saveAndExecuteEvent")
    @ResponseBody
    public BaseBean saveAndExecuteEvent(String event){
        MessageBean result=new MessageBean();
        computerControlService.executeEvent(event);
        result.setData(true);
        return result;
    }
}
