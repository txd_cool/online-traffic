package cn.xchats.onlinetraffic.computer.control.server.service;

import cn.xchats.onlinetraffic.common.other.redis.RedisTemplateService;
import cn.xchats.onlinetraffic.common.type.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/*
 *
 *@author teddy
 *@date 2018/5/29
 */
@Service
public class ComputerControlService implements ComputerControlInterface{

    @Autowired
    private RedisTemplateService redisTemplateService;

    private static Robot computer;

    public ComputerControlService() {
        computer=newRobot();
    }

    public void executeEvent(String event){
        executeEventInterface(computer,event);
    }




}
